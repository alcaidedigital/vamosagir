<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/



$route['(:any).php'] = 'redirect/page/$1';

$route['default_controller'] = 'home';
$route['404_override'] = '';

$route['agir'] = 'act';
$route['agir/conte-o-que-voce-fez'] = 'act/youdid';
$route['agir/inspiracao/?(:any)?/?(:any)?/?'] = 'act/inspiration/$1/$2';
$route['agir/o-que-estamos-fazendo'] = 'act/whatwearedoing';

$route['compartilhe'] = 'share';
$route['compartilhe/brasil-agindo'] = 'share/acting';

$route['sucesso'] = 'success';
$route['noticias'] = 'news';
$route['material-de-campanha'] = 'material';
$route['material-de-campanha/locais-para-retirada'] = 'material/places';
$route['material-de-campanha/imprima'] = 'material/printout';
$route['material-de-campanha/seja-um-distribuidor'] = 'material/bedistributor';
$route['material-de-campanha/receba-em-casa'] = 'material/receive';
$route['conheca'] = 'know';
$route['legislacao'] = 'law';
$route['documentos'] = 'document';
$route['documentos/?(:num)?/?(:any)?/?'] = 'document/detail/$1/$2';
$route['noticias'] = 'news';
$route['noticias/?(:num)?/?(:any)?/?'] = 'news/detail/$1/$2';


//admin
$route['adm'] = 'adm/adm';
$route['adm/banner/novo'] = 'adm/banner/edit';
$route['adm/banner/editar(/)?(\d+)?'] = 'adm/banner/edit/$2';
$route['adm/banner/delete(/)?(\d+)?'] = 'adm/banner/delete/$2';
$route['adm/banner/record(/)?(\d+)?'] = 'adm/banner/record/$2';

$route['adm/noticias'] = 'adm/news';
$route['adm/noticias/novo'] = 'adm/news/edit';
$route['adm/noticias/editar(/)?(\d+)?'] = 'adm/news/edit/$2';
$route['adm/noticias/delete(/)?(\d+)?'] = 'adm/news/delete/$2';
$route['adm/noticias/record(/)?(\d+)?'] = 'adm/news/record/$2';

$route['adm/noticias/imagens/(\d+)']                = 'adm/newsimage/index/$1';
$route['adm/noticias/imagens/delete/(\d+)/(\d+)']   = 'adm/newsimage/delete/$1/$2';
$route['adm/noticias/imagens/record(/)?(\d+)?']     = 'adm/newsimage/record/$2';

$route['adm/noticias/youtube/(\d+)']                = 'adm/newsyoutube/index/$1';
$route['adm/noticias/youtube/delete/(\d+)/(\d+)']   = 'adm/newsyoutube/delete/$1/$2';
$route['adm/noticias/youtube/record(/)?(\d+)?']     = 'adm/newsyoutube/record/$2';

$route['adm/documentos'] = 'adm/document';
$route['adm/documentos/novo'] = 'adm/document/edit';
$route['adm/documentos/editar(/)?(\d+)?'] = 'adm/document/edit/$2';
$route['adm/documentos/delete(/)?(\d+)?'] = 'adm/document/delete/$2';
$route['adm/documentos/record(/)?(\d+)?'] = 'adm/document/record/$2';

$route['adm/conte-o-que-voce-fez'] = 'adm/youdid';
$route['adm/conte-o-que-voce-fez/editar(/)?(\d+)?'] = 'adm/youdid/edit/$2';
$route['adm/conte-o-que-voce-fez/delete(/)?(\d+)?'] = 'adm/youdid/delete/$2';
$route['adm/conte-o-que-voce-fez/excel(/)?'] = 'adm/youdid/excel';

$route['adm/receba-em-casa'] = 'adm/receiver';
$route['adm/receba-em-casa/editar(/)?(\d+)?'] = 'adm/receiver/edit/$2';
$route['adm/receba-em-casa/delete(/)?(\d+)?'] = 'adm/receiver/delete/$2';

$route['adm/vem-fazer-diferente'] = 'adm/bepart';
$route['adm/vem-fazer-diferente/editar(/)?(\d+)?'] = 'adm/bepart/edit/$2';
$route['adm/vem-fazer-diferente/delete(/)?(\d+)?'] = 'adm/bepart/delete/$2';

$route['adm/seja-um-distribuidor'] = 'adm/bedistributor';
$route['adm/seja-um-distribuidor/editar(/)?(\d+)?'] = 'adm/bedistributor/edit/$2';
$route['adm/seja-um-distribuidor/delete(/)?(\d+)?'] = 'adm/bedistributor/delete/$2';

$route['adm/receba-em-casa'] = 'adm/receive';
$route['adm/receba-em-casa/editar(/)?(\d+)?'] = 'adm/receive/edit/$2';
$route['adm/receba-em-casa/delete(/)?(\d+)?'] = 'adm/receive/delete/$2';





/* End of file routes.php */
/* Location: ./application/config/routes.php */
