<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Document_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get(array $where = array())
    {
        $this->db->order_by('date', 'DESC')
            ->order_by('id', 'DESC');
        return parent::get($where);
    }

}

/* End of file document_model.php */
/* Location: ./application/models/document_model.php */