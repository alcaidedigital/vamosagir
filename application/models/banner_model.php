<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Banner_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get(array $where = array())
    {
        $this->db->order_by('order');
        return parent::get($where);
    }
}

/* End of file banner_model.php */
/* Location: ./application/models/banner_model.php */