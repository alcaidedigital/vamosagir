<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class News_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get(array $where = array())
    {
        $this->db->order_by('date', 'DESC')
            ->order_by('id', 'DESC');
        return parent::get($where);
    }
    public function featured(array $where = array())
    {
        $this->db->limit(3);
        return $this->get($where);
    }
}

/* End of file news_model.php */
/* Location: ./application/models/news_model.php */