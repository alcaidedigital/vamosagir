<?php
/*
/*
 * Method to load css files into your project.
 * @param array $arrFile
 * @param string $path
 */
if (!function_exists('loadCss')) {
    function loadCss($arrFile, $path = NULL)
    {
        if (!is_array($arrFile)) {
            $arrFile = (array)$arrFile;
        }
        $return = '';
        foreach ($arrFile as $file) {
            $fileNoMin = 'assets/' . $path . 'css/' . $file . '.css';
            $fileMin = 'assets/' . $path . 'css/' . $file . '.min.css';
            if (file_exists(ROOT_PATH . DIRECTORY_SEPARATOR . $fileNoMin) === TRUE) {
                $CI =& get_instance();
                if (ENVIRONMENT == 'development' ||
                    file_exists(ROOT_PATH . DIRECTORY_SEPARATOR . $fileMin) === FALSE
                    || isset($CI->data['logado']->id) === true
                ) {
                    $CI->load->driver('minify');
                    $CI->load->helper('file');
                    $contents = read_file($fileNoMin);
                    $contents = $CI->minify->css->min($contents);
                    write_file($fileMin, $contents);
                }
                $return .= '<link rel="stylesheet" type="text/css" href="./' . $fileMin . '"/>' . PHP_EOL;
            } else {
                log_message('debug', __METHOD__ . ROOT_PATH . DIRECTORY_SEPARATOR . $fileNoMin . ' does not exists');
            }
        }
        return $return;
    }
}
//
/*
 * Method to load javascript files into your project.
 * @param array $js
 */
if (!function_exists('loadJs')) {
    function loadJs($js, $path = NULL)
    {
        if (!is_array($js)) {
            $js = (array)$js;
        }
        $return = '';
        foreach ($js as $j) {
            $j = 'assets/' . $path . 'js/' . $j . '.js';
            if (file_exists(ROOT_PATH . DIRECTORY_SEPARATOR . $j) === TRUE) {
                $return .= '<script type="text/javascript" src="./' . $j . '"></script>' . PHP_EOL;
            } else {
                log_message('debug', __METHOD__ . ROOT_PATH . DIRECTORY_SEPARATOR . $j . ' does not exists');
            }
        }
        return $return;
    }
}

/*
 * Method to load dataTable into your project.
 */
if (!function_exists('loadDataTable')) {
    function loadDataTable()
    {
        $return = '<link type="text/css" rel="stylesheet" href="./assets/dataTables/media/css/jquery.dataTables.css" />' . PHP_EOL;
        $return .= '<script type="text/javascript" src="./assets/dataTables/media/js/jquery.dataTables.min.js"></script>' . PHP_EOL;
        $return .= '<script type="text/javascript" src="./assets/dataTables/dataTables.js"></script>' . PHP_EOL;
        return $return;
    }
}
/*
 * Method to load the shadowbox into your project.
 */
if (!function_exists('loadFancybox')) {
    function loadFancybox()
    {
        $return = '<script type="text/javascript" src="./assets/fancybox/jquery.fancybox.js?v=2.1.5"></script>' . PHP_EOL;
        $return .= '<link rel="stylesheet" type="text/css" href="./assets/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />' . PHP_EOL;
        //Add Button helper (this is optional)
        $return .= '<link rel="stylesheet" type="text/css" href="./assets/fancybox/jquery.fancybox-buttons.css?v=1.0.5" />' . PHP_EOL;
        $return .= '<script type="text/javascript" src="./assets/fancybox/jquery.fancybox-buttons.js?v=1.0.5"></script>' . PHP_EOL;
        //Add Thumbnail helper (this is optional)
        $return .= '<link rel="stylesheet" type="text/css" href="./assets/fancybox/jquery.fancybox-thumbs.css?v=1.0.7" />' . PHP_EOL;
        $return .= '<script type="text/javascript" src="./assets/fancybox/jquery.fancybox-thumbs.js?v=1.0.7"></script>' . PHP_EOL;
        //Add Media helper (this is optional)
        $return .= '<script type="text/javascript" src="./assets/fancybox/jquery.fancybox-media.js?v=1.0.6"></script>' . PHP_EOL;
        $return .= '<script type="text/javascript">' . PHP_EOL;
        $return .= '$(document).ready(function() {' . PHP_EOL;
        $return .= '$(".fancybox").fancybox();' . PHP_EOL;
        $return .= '$(".video").click(function() {' . PHP_EOL;
        $return .= '$.fancybox({' . PHP_EOL;
        $return .= '"href"			: this.href.replace(new RegExp("watch\\\?v=", "i"), "v/"),' . PHP_EOL;
        $return .= '"type"			: "swf",' . PHP_EOL;
        $return .= '"swf"			: {' . PHP_EOL;
        $return .= '"wmode"				: "transparent",' . PHP_EOL;
        $return .= '"allowfullscreen"	: "true"' . PHP_EOL;
        $return .= '}' . PHP_EOL;
        $return .= '});' . PHP_EOL;
        $return .= 'return false;' . PHP_EOL;
        $return .= '});' . PHP_EOL;
        $return .= '});' . PHP_EOL;
        $return .= '</script>' . PHP_EOL;
        return $return;
    }
}
/*
 * Method to load 3DGallery into your project.
 */
if (!function_exists('load3DGallery')) {
    function load3DGallery()
    {
        $return = '<link type="text/css" rel="stylesheet" href="./assets/3DGallery/css/style.css" />' . PHP_EOL;
        $return .= '<script type="text/javascript" src="./assets/3DGallery/js/modernizr.custom.53451.js"></script>' . PHP_EOL;
        $return .= '<script type="text/javascript" src="./assets/3DGallery/js/jquery.gallery.js"></script>' . PHP_EOL;
        return $return;
    }
}

/*
 * Method to load dataTable into your project.
 */
if (!function_exists('loadJcarousel')) {
    function loadJcarousel(array $options = array())
    {
        $return = '<link rel="stylesheet" href="./assets/jcarousel/dist/core.css" />' . PHP_EOL;
        $jcarousel['mim'] = '<script type="text/javascript" src="./assets/jcarousel/dist/jquery.jcarousel.min.js"></script>' . PHP_EOL;
        $jcarousel['autoscroll'] = '<script type="text/javascript" src="./assets/jcarousel/dist/jquery.jcarousel.min.js"></script>' . PHP_EOL;

        $return .= $jcarousel['mim'];
        foreach ($options as $row) {
            $return .= $jcarousel[$row];
        }

        return $return;
    }
}

/*
 * Method to load dataTable into your project.
 */
if (!function_exists('loadCkeditor')) {
    function loadCkeditor()
    {
        return '<script src="./assets/ckeditor/ckeditor.js" type="text/javascript" charset="utf-8"></script>' . PHP_EOL;
    }
}

/*
 * Method to load validator lib into your project.
 */
if (!function_exists('loadValidator')) {
    function loadValidator()
    {
        $return = '<link rel="stylesheet" href="./assets/form-validator/style.css" />' . PHP_EOL;
        $return .= '<script src="./assets/form-validator/jquery.form-validator.js" type="text/javascript" charset="utf-8"></script>' . PHP_EOL;
        return $return;
    }
}

/*
 * Method to load masks lib into your project.
 */
if (!function_exists('loadMask')) {
    function loadMask()
    {
        $return = '<script src="./assets/jquery.mask/jquery.mask.min.js" type="text/javascript" charset="utf-8"></script>' . PHP_EOL;
        return $return;
    }
}