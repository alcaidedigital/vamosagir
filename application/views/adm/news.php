<div class="new-record">
    <a href="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/novo"/>
    Novo
    </a>
</div>
<table class="dataTable">
    <thead>
    <tr>
        <th>Data</th>
        <th>Título</th>
        <th>Local</th>
        <th style="width:80px"></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($list as $row) : ?>
        <tr>
            <td><?php echo $row->date ?></td>
            <td><?php echo $row->title; ?></td>
            <td><?php echo $row->local; ?></td>
            <td>
                <a href="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/youtube/<?php echo $row->id; ?>" title="Youtube">
                    <img src="./assets/<?php echo $this->uri->segment(1); ?>/img/youtube.png"/>
                </a>
                <a href="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/imagens/<?php echo $row->id; ?>" title="Imagens">
                    <img src="./assets/<?php echo $this->uri->segment(1); ?>/img/images.png"/>
                </a>
                <a href="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/editar/<?php echo $row->id; ?>">
                    <img src="./assets/<?php echo $this->uri->segment(1); ?>/img/edit.png"/>
                </a>
                <a href="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/delete/<?php echo $row->id; ?>">
                    <img src="./assets/<?php echo $this->uri->segment(1); ?>/img/delete.png"/>
                </a>
            </td>
        </tr>
    <?php endForeach; ?>
    </tbody>
</table>