		<h1>Conte o que você fez</h1>
		<form action="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/record/<?php echo isset($data->id) ? $data->id : NULL ?>" method="post" enctype="multipart/form-data" class="form">
            <table>
                <tbody>
                <tr>
                    <td valign="top">
                        <label for="name">Name</label>
                    </td>
                    <td>
                        <input type="text" name="name" id="name" value="<?php echo isset($data->name) ? $data->name: NULL ?>" /><br />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <label for="email">E-mail</label>
                    </td>
                    <td>
                        <input type="text" name="email" id="email" value="<?php echo isset($data->email) ? $data->email: NULL ?>" /><br />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <label for="rg">RG</label>
                    </td>
                    <td>
                        <input type="text" name="rg" id="rg" value="<?php echo isset($data->rg) ? $data->rg: NULL ?>" /><br />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <label for="cpf">CPF</label>
                    </td>
                    <td>
                        <input type="text" name="cpf" id="cpf" value="<?php echo isset($data->cpf) ? $data->cpf: NULL ?>" /><br />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <label for="phone">Telefone</label>
                    </td>
                    <td>
                        <input type="text" name="phone" id="phone" value="<?php echo isset($data->phone) ? $data->phone: NULL ?>" /><br />
                    </td>
                </tr>
                </tbody>
            </table>
		</form> 