		<h1>Receba em casa</h1>
		<form action="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/record/<?php echo isset($data->id) ? $data->id : NULL ?>" method="post" enctype="multipart/form-data" class="form">
            <table>
                <tbody>
                <tr>
                    <td valign="top">
                        <label for="name">Name</label>
                    </td>
                    <td>
                        <input type="text" name="name" id="name" value="<?php echo isset($data->name) ? $data->name: NULL ?>" /><br />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <label for="email">E-mail</label>
                    </td>
                    <td>
                        <input type="text" name="email" id="email" value="<?php echo isset($data->email) ? $data->email: NULL ?>" /><br />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <label for="cpf">CPF</label>
                    </td>
                    <td>
                        <input type="text" name="cpf" id="cpf" value="<?php echo isset($data->cpf) ? $data->cpf: NULL ?>" /><br />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <label for="address">Endereço</label>
                    </td>
                    <td>
                        <input type="text" name="address" id="address" value="<?php echo isset($data->address) ? $data->address: NULL ?>" />
                        <input type="text" name="number" id="number" value="<?php echo isset($data->number) ? $data->number: NULL ?>" /><br />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <label for="complement">Complemento</label>
                    </td>
                    <td>
                        <input type="text" name="complement" id="complement" value="<?php echo isset($data->complement) ? $data->complement : NULL ?>" /><br />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <label for="squarestickers">Adesivos </label>
                    </td>
                    <td>
                        <input type="text" name="squarestickers" id="squarestickers" value="<?php echo isset($data->squarestickers) ? $data->squarestickers : NULL ?>" /><br />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <label for="roundstickers">Bótons</label>
                    </td>
                    <td>
                        <input type="text" name="roundstickers" id="roundstickers" value="<?php echo isset($data->roundstickers) ? $data->roundstickers : NULL ?>" /><br />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <label for="action">Ação</label>
                    </td>
                    <td>
                        <textarea name="action" id="action" cols="30" rows="10"><?php echo isset($data->action) ? $data->action: NULL ?></textarea><br />
                    </td>
                </tr>
                </tbody>
            </table>
		</form> 