		<h1><?php echo isset($data->id) ? 'Editar' : 'Novo' ?> Banner</h1>
		<form action="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/record/<?php echo isset($data->id) ? $data->id : NULL ?>" method="post" enctype="multipart/form-data" class="form">
            <table>
                <tbody>
                    <tr>
                        <td valign="top">
                            <label for="link">Link</label>
                        </td>
                        <td>
                            <input type="text" name="link" id="link" value="<?php echo isset($data->link) ? $data->link : NULL ?>" /><br />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <label for="img">Image</label>
                        </td>
                        <td>
                            <input type="file" id="img" name="img" /><br />
                            <?php if(isset($data->img)): ?>
                            <label for="img">
                                <img src="<?php echo $data->img ?>" class="preview" />
                            </label>
                            <?php endif ?>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                        <td colspan="2"><input type="submit" value="Salvar" /></td>
                    </tr>
                </tbody>
            </table>
		</form> 