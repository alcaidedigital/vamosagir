		<h1>Conte o que você fez</h1>
		<form action="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/record/<?php echo isset($data->id) ? $data->id : NULL ?>" method="post" enctype="multipart/form-data" class="form">
            <table>
                <tbody>
                <tr>
                    <td valign="top">
                        <label for="name">Name</label>
                    </td>
                    <td>
                        <input type="text" name="name" id="name" value="<?php echo isset($data->name) ? $data->name: NULL ?>" /><br />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <label for="city">Cidade</label>
                    </td>
                    <td>
                        <input type="text" name="city" id="city" value="<?php echo isset($data->city) ? $data->city: NULL ?>" /><br />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <label for="zipcode">CEP</label>
                    </td>
                    <td>
                        <input type="text" name="zipcode" id="zipcode" value="<?php echo isset($data->zipcode) ? $data->zipcode: NULL ?>" /><br />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <label for="email">E-mail</label>
                    </td>
                    <td>
                        <input type="text" name="email" id="email" value="<?php echo isset($data->email) ? $data->email: NULL ?>" /><br />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <label for="action">Ação</label>
                    </td>
                    <td>
                        <textarea name="action" id="action" cols="30" rows="10"><?php echo isset($data->action) ? $data->action: NULL ?></textarea><br />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <label for="file">Arquivo</label>
                    </td>
                    <td>
                        <a target="_blank" href="<?php echo isset($data->file) ? $data->file: NULL ?>"><?php echo isset($data->file) ? $data->file: NULL ?></a>
                    </td>
                </tr>
                </tbody>
            </table>
		</form> 