		<h1>Cadastrar imagens</h1>
		<form action="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/<?php echo $this->uri->segment(3); ?>/record/<?php echo $id ?>" method="post" enctype="multipart/form-data" class="form">
            <table>
                <tbody>
                    <tr>
                        <td valign="top">
                            <label for="src">Imagem</label>
                        </td>
                        <td>
                            <input type="file" tabindex="1" id="src" name="src[]" multiple accept="image/*" /><br />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                        <td colspan="2"><input tabindex="2" type="submit" value="Salvar" /></td>
                    </tr>
                </tbody>
            </table>
		</form>
        <form action="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/<?php echo $this->uri->segment(3); ?>/record/<?php echo $id ?>" method="post" class="form">
            <div class="list">
                <?php foreach($list as $row):?>
                <div class="item">
                    <div class="order">
                        <label for="id_<?php echo $row->id ?>">Ordem</label>
                        <input tabindex="1<?php echo $row->id ?>" type="number" name="id[<?php echo $row->id ?>]" id="id_<?php echo $row->id ?>" value="<?php echo $row->order ?>" />
                    </div>
                    <a href="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/<?php echo $this->uri->segment(3); ?>/delete/<?php echo $id ?>/<?php echo $row->id ?>">
                        <img src="<?php echo $row->src ?>">
                    </a>
                </div>
                <?php endforeach;?>
            </div>
            <?php if(count($list) > 0):?>
                <input type="submit" value="Ordenar" />
            <?php endif; ?>
        </form>