<h1><?php echo isset($data->id) ? 'Editar' : 'Nova' ?> Notícia</h1>
<form
    action="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/record/<?php echo isset($data->id) ? $data->id : NULL ?>"
    method="post" enctype="multipart/form-data" class="form">
    <table>
        <tbody>
        <tr>
            <td valign="top">
                <label for="name">Título</label>
            </td>
            <td>
                <input type="text" name="title" id="title"
                       value="<?php echo isset($data->title) ? $data->title : NULL ?>"/><br/>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <label for="local">Local</label>
            </td>
            <td>
                <input type="text" name="local" id="local"
                       value="<?php echo isset($data->local) ? $data->local : NULL ?>"/><br/>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <label for="date">Data</label>
            </td>
            <td>
                <input type="text" name="date" id="date"
                       value="<?php echo isset($data->date) ? $data->date : date('d/m/Y') ?>"/><br/>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <label for="thumb">Miniatura</label>
            </td>
            <td>
                <input type="file" id="thumb" name="thumb"/><br/>
                <?php if (isset($data->thumb)): ?>
                    <label for="thumb">
                        <img src="<?php echo $data->thumb; ?>" class="preview"/>
                    </label>
                <?php endif ?>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <label for="text">Texto</label>
            </td>
            <td>
                <textarea name="text" id="text" class="ckeditor"><?php echo isset($data->text) ? $data->text : NULL ?></textarea><br/>
            </td>
        </tr>
        <tr>
            <td valign="top">
            <td colspan="2"><input type="submit" value="Salvar"/></td>
        </tr>
        </tbody>
    </table>
</form>