            <table class="dataTable">
                  <thead>
                        <tr>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>CPF</th>
                            <th>Endereço</th>
                            <th>Complemento</th>
                            <th>Button</th>
                            <th>Adesivos</th>
                            <th style="width:37px"></th>
                        </tr>
                  </thead>
                  <tbody>
                        <?php foreach ($list as $row) :?>
                        <tr>
                            <td><?php echo $row->name; ?></td>
                            <td><?php echo $row->email; ?></td>
                            <td><?php echo $row->cpf; ?></td>
                            <td><?php echo $row->address; ?>,<?php echo $row->number; ?></td>
                            <td><?php echo $row->complement; ?></td>
                            <td><?php echo $row->roundstickers; ?></td>
                            <td><?php echo $row->squarestickers; ?></td>
                              <td>
                                    <a href="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/editar/<?php echo $row->id; ?>">
                                          <img src="./assets/<?php echo $this->uri->segment(1); ?>/img/edit.png" />
                                    </a>
                                    <a href="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/delete/<?php echo $row->id; ?>">
                                          <img src="./assets/<?php echo $this->uri->segment(1); ?>/img/delete.png" />
                                    </a>
                              </td>
                        </tr>
                        <?php endForeach;?>
                  </tbody>
            </table>