<div id="blue-line">
    <a href="./<?php echo $this->uri->segment(1); ?>" class="title">     Material de Campanha</a>
</div>
<div id="main">
    <h1>Imprima nosso material</h1>
    <p>Comece a divulgar as ideias por um Brasil diferente. Vamos mudar a cara do nosso país e faremos isso com nossas próprias mãos.</p>
    <div class="link">
        <img src="./assets/img/material/call.jpg" width="634" height="205" class="call">
        <a href="./<?php echo $this->uri->segment(1)?>/receba-em-casa">
            <img src="./assets/img/material/receive.jpg" height="205" width="310" class="scale-1-7">
        </a>
        <a href="./<?php echo $this->uri->segment(1)?>/seja-um-distribuidor">
            <img src="./assets/img/material/distributor.jpg" height="205" width="310" class="scale-1-7">
        </a>
        <a href="./<?php echo $this->uri->segment(1)?>/locais-para-retirada">
            <img src="./assets/img/material/places.jpg" height="205" width="310" class="scale-1-7">
        </a>
        <a href="./<?php echo $this->uri->segment(1)?>/imprima">
            <img src="./assets/img/material/print.jpg" height="205" width="310" class="scale-1-7">
        </a>
    </div>
</div>