<div id="blue-line">
    <a href="./<?php echo $this->uri->segment(1); ?>" class="title">Conheça</a>
</div>
<div id="main">
    <h1>Nossa bandeira</h1>
    <p>Durante a campanha do Aécio Neves, queremos inspirar uma nova geração de empreendedores cívicos que vão trabalhar ativamente na mudança do Brasil.</p>
    <p>Estamos prontos para agir. As ações de transformação do país já começaram.</p>
    <p>Pense em ideias para a sua cidade ou comunidade. Convide todos os seus amigos, converse com eles pelas esquinas, pelo seu Facebook e por todas as suas redes sociais. Bem-vindo. </p>
    <h1>Nossos princípios</h1>
    <p>
        Não violência<br />
        Fibra ética<br />
        Trabalhar com alegria<br />
        Criação de equipes<br />
        Autonomia<br />
        Promover a ação e deixar um legado<br />
        Pluralismo<br />
    </p>
</div>