<div id="blue-line">
    <a href="./<?php echo $this->uri->segment(1); ?>" class="title">Compartilhe</a>
</div>
<div id="main">
    <h1>O Brasil agindo</h1>
    <p>Esperamos as suas experiências para compartilhar aqui. <a href="./agir/conte-o-que-voce-fez"> Conte para nós</a> o que você está fazendo.</p>
    <div class="share">
        <span class="text">Compartilhar</span>
        <a href="javascript: void(0);" onClick="window.open('http://www.facebook.com/sharer.php?u=<?php echo current_url();?>','Share', 'toolbar=0, status=0, width=650, height=450');">
            <img src="./assets/img/socialicon/fb.png" height="22" width="22">
        </a>
        <a href="javascript: void(0);" onClick="window.open('http://twitter.com/intent/tweet?source=sharethiscom&url=<?php echo current_url();?>','Share', 'toolbar=0, status=0, width=650, height=450');">
            <img src="./assets/img/socialicon/twitter.png" height="22" width="22">
        </a>
        <a href="javascript: void(0);" onClick="window.open('https://plus.google.com/share?url=<?php echo current_url();?>','Share', 'toolbar=0, status=0, width=650, height=450');">
            <img src="./assets/img/socialicon/plus.png" height="22" width="22">
        </a>
    </div>
    <div class="link">
    <?php foreach($img as $row):?>
        <a class="fancybox" href="<?php echo $row;?>" data-fancybox-group="a">
            <img src="<?php echo $row;?>" />
        </a>
    <?php endforeach;?>
    </div>
</div>