<div id="blue-line">
    <a href="./<?php echo $this->uri->segment(1); ?>" class="title">Agir</a>
</div>
<div id="main">
    <h1>O que estamos fazendo</h1>
    <p>Muita gente já se juntou para a mudança acontecer. Estamos por todos os lados em busca de um país novo, mais solidário e melhor. Confira aqui um pouco dessa transformação.</p>
    <div class="link">

        <a href="./noticias/8/adesivaco-e-limpeza-no-estado-de-sao-paulo">
            <img src="./assets/img/act/whatwearedoing/sampa.jpg" height="203" width="509" class="scale-1-7">
        </a>
        <a href="./noticias/11/venham-para-a-limpeza-na-lagoa-de-marapendi">
            <img src="./assets/img/act/whatwearedoing/marapendi.jpg" height="203" width="509" class="scale-1-7">
        </a>
        <a href="./noticias/13/carreata-em-belo-horizonte">
            <img src="./assets/img/act/whatwearedoing/carreatabh.jpg" height="203" width="509" class="scale-1-7">
        </a>
        <a href="./noticias/10/grande-acao-dos-voluntarios-em-minas-gerais">
            <img src="./assets/img/act/whatwearedoing/minas.jpg" height="203" width="509" class="scale-1-7">
        </a>

        <a href="./noticias/4/adesivaco-em-niteroi">
            <img src="./assets/img/act/whatwearedoing/pracaxv.jpg" height="203" width="509" class="scale-1-7">
        </a>
        <a href="./noticias/1/limpeza-e-adesivao-no-largo-do-machado">
            <img src="./assets/img/act/whatwearedoing/largomachado.jpg" height="203" width="509" class="scale-1-7">
        </a>
        <a href="./noticias/3/todos-os-estados-se-reuniram-para-discutir-o-voluntariado">
            <img src="./assets/img/act/whatwearedoing/encontro.jpg" height="203" width="509" class="scale-1-7">
        </a>
    </div>
</div>