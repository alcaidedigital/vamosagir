<div id="blue-line">
    <a href="./<?php echo $this->uri->segment(1); ?>" class="title">Material de Campanha</a>
</div>
<div id="main">
    <h1>Locais para retirada</h1>
    <h2>REGIÃO  SUDESTE</h2>
    <h3>ESPIRITO SANTO </h3>

    <p>Cariacica<br>
        Espirito Santo<br>
        Rua João Bonadiman,  nº2<br>
        Nilton Basílio  Teixeira<br>
        (27) 99506-2123<br>
        abv.siqueira@live.com</p>
    <h3>MINAS GERAIS</h3>

    <p>MG<br>
        Belo  Horizonte<br>
        Avenida Augusto de Lima, 30 <br>
        Isadora Sabino de  Freitas<br>
        32-8806.8808<br>
        isadorasabino@gmail.com</p>

    <p>MG<br>
        Conselheiro  Lafaiete<br>
        Rua Afonso Pena, 262<br>
        Moisés Matias<br>
        31-8726.0430 <br>
        31-9521.6566<br>
        moisesmatiaspereira@hotmail.com</p>

    <p>MG<br>
        Curvelo<br>
        Rua  Morada Nova de Minas ,138 <br>
        Adair Divino da Silva(Bem Ti  Vi)<br>
        38-9911 4545<br>
        bemtivi3m@hotmail.com</p>

    <p>MG<br>
        Diamantina<br>
        Praça  da Luz, 241<br>
        Roberto Grapiúna<br>
        31-9977  7252<br>
        robertograpiuna@gmail.com</p>

    <p>MG<br>
        Divinópolis<br>
        Alameda  do Sabiá,305<br>
        José Kenedy Ribeiro (Kendão)<br>
        37-9967  1048<br>
        kendao45@yahoo.com.br</p>

    <p>MG<br>
        Governador  Valadares<br>
        Alameda Mariquita Peres, 540 - Condominio Jother  Peres<br>
        Patricia<br>
        33-9102 7906<br>
        patyrocha67@hotmail.com</p>

    <p>MG<br>
        Governador  Valadares<br>
        Avenida Tancredo Neves, 2115 <br>
        Ernani <br>
        33-9918 7673<br>
        33-9917  2666<br>
        patyrocha67@hotmail.com<br>
        MG<br>
        Cômite  BH</p>


    <p>MG<br>
        Juiz de  Fora<br>
        Rua Matinho Gonçalves, 229 - <br>
        Paulo Mendes<br>
        32-9129  4072<br>
        paulo.mendessoares@yahoo.com.br</p>


    <p>MG<br>
        Lavras<br>
        Rua  Praça Dr Augusto Silva, 716 Sala 01<br>
        Aristides Monteiro Neto (  Tide)<br>
        35-9964 5030<br>
        arimone2008@gmail.com</p>

    <p>MG<br>
        Montes  Claros<br>
        Av. Dep. Esteves Rodrigues,129<br>
        Valmir<br>
        38-9949  3680<br>
        &quot;beatrizmorais77@gmail.com<br>
        bernadete.andrade1@gmail.com&quot;</p>

    <p>MG<br>
        Pouso  Alegre<br>
        Rua Vereador Valdomiro Bueno,188<br>
        Celso Eliais da  Silveira<br>
        35-8882 5418<br>
        celsosilveira2003@yahoo.com.br</p>

    <p>MG<br>
        São João del  Rei<br>
        Rua Maria Tereza,110<br>
        Altamiro<br>
        32-9967  7021<br>
        altamiro63@hotmail.com</p>

    <p>MG<br>
        Teófilo  Otoni<br>
        Rua Padre Virgulino,668<br>
        José Aécio<br>
        33-8885  0946<br>
        aeciosantosladainha@hotmail.com</p>


    <p>MG<br>
        Ubá<br>
        Av.  Governador Valadares,978<br>
        Francisco da Silva (Kiko)<br>
        32-8841  0966<br>
        kiko@garcia.art.br</p>

    <p>MG<br>
        Uberaba<br>
        Av.  Leopoldino de Oliveira,3024<br>
        Carlos Wazir</p>
    <p>Tiago Ganso<br>
        34-9167  0437<br>
        thiagotiveron@hotmail.com</p>

    <p>MG<br>
        Cômite  BH<br>
        Avenida Augusto de Lima, 30<br>
        CEP 30.140.001<br>
        Gustavo  Paoliello<br>
        31-9798 2073<br>
        gustavopaoliello2@yahoo.com.br</p>

    <p>MG<br>
        Cômite  BH<br>
        Avenida Augusto de Lima, 30<br>
        CEP 30.140.001<br>
        Hélio  Campos<br>
        31-8335 0977<br>
        heliomarciocamposob@hotmail.com</p>

    <p>MG<br>
        Cômite  BH<br>
        Avenida Augusto de Lima, 30<br>
        CEP 30.140.001<br>
        Joanna  Somers<br>
        31-9892 8653<br>
        joannasomers@gmail.com</p>

    <h3>RIO DE JANEIRO</h3>

    <p>Barra Mansa<br>
        Rio de Janeiro<br>
        Rua Orlando Brandão,  nº 123<br>
        Ricardo Volp Maciel <br>
        (12)98250-0289</p>
    <p>  Cabo Frio <br>
        Rio de Janeiro <br>
        Rua Carlos Mendes,  nº 28 <br>
        Vanderlei Rodrigues  Bento<br>
        (22) 2647-3636<br>
        gel@censa.com.br</p>

    <p>Macaé<br>
        Rio de Janeiro<br>
        Rua Niobel Sardinha,  nº 99<br>
        Adeilton da Silva  Santos <br>
        (22) 2770-4444<br>
        psdbmacae@gmail.com</p>

    <p>Magé<br>
        Rio de Janeiro<br>
        Rua Capitão Caio  Fonseca Ramos, nº 15 <br>
        Edivar Souza Tavares<br>
        (21) 2633-3804<br>
        abv.edivar@ig.com.br</p>
    <p>Mesquita <br>
        Rio de Janeiro<br>
        Rua Licínio, nº  284 <br>
        Raphael Duarte  Mourão <br>
        (21) 2796-7326 <br>
        corricanst@hotmail.com</p>
    <p>Resende<br>
        Rio de Janeiro<br>
        Rua Cláudio Manoel  da Costa, nº 21<br>
        Fernando Henrique  Soares <br>
        (24) 3354-1414<br>
        herifer@gmail.com</p>
    <p>São Gonçalo <br>
        Rio de Janeiro <br>
        Travessa São  Gonçalo, nº 45<br>
        Michel Portugal <br>
        (21) 2604-6605<br>
        tpnilsea@gmail.com<br>
    </p>
    <p>Teresópolis <br>
        Rio de Janeiro<br>
        Rua Cecília  Meireles, nº 557 <br>
        Alexandre Pain <br>
        (21) 2642-3749<br>
        pain_alexandre@hotmail.com</p>



    <h3>SÃO PAULO</h3>

    <p>Araçatuba<br>
        São  Paulo<br>
        RUA JOSÉ BONIFÁCIO 335<br>
        Manoel Afonso de Oliveira  Filho<br>
        (18) 98114-0033<br>
        maafilho@terra.com.br</p>

    <p> Atibaia<br>
        São  Paulo<br>
        ESTRADA DOS PIRES, 500<br>
        Fernando Emanuel Mamede<br>
        (11)  99902-0563<br>
        fernando@mamede.com.br/ atibaiapsdb@gmail.com</p>

    <p> Caraguatatuba<br>
        São Paulo<br>
        AV. VITAL BRASIL, 130<br>
        Lucio Fernandes<br>
        (12)  7813-8434<br>
        luciofernandes@hotmail.com</p>

    <p> Catanduva<br>
        São  Paulo<br>
        RUA AMAZONAS, 290 APTO 62<br>
        Tales Rene Campanha Cury<br>
        (17)  99717-6055<br>
        talescury@uol.com.br</p>

    <p> Embu das Artes<br>
        São Paulo<br>
        RUA JUSSARA, 90<br>
        José Carlos Ferreira de  Proença<br>
        (11)  99998-0092/4701-3550/3433-9209<br>
        carlaoproenca@uol.com.br</p>

    <p> Francisco Morato<br>
        São Paulo<br>
        RUA JOAO MENDES JUNIOR, 85/ Rual Azevedo marques 85,  centro francisco morato<br>
        Silverio Jose Pelizari Pinto<br>
        (11)  97313-7579<br>
        tucomorato@ig.com.br</p>

    <p> Guarulhos<br>
        São  Paulo<br>
        RUA FRITZ REIMANN, 470<br>
        Aurea Amanda Guerreiro de  Campos<br>
        (110 99651-9303<br>
        amandacamposjuridico@gmail.com</p>

    <p> Itapetininga<br>
        São Paulo<br>
        DRº COUTINHO 733<br>
        Luis Antonio di Fiori Costa<br>
        (11)  99625-3323<br>
        psdb45.itapetininga@gmail.com</p>

    <p> Itapevi<br>
        São  Paulo<br>
        RUA ANDRE CAVANHA, 305<br>
        Valter Francisco Antonio<br>
        (11)  97422-1788<br>
        alexandrecamarg@hotmail.com</p>

    <p> Itu<br>
        São  Paulo<br>
        RUA ANTONIO TOLEDO PIZA, ALAMEDA 01 - SL 01<br>
        Antonio  Claudio de Arruda Campos<br>
        (11)  97151-0748<br>
        caetano@alvoradasupermercado.com</p>

    <p> Jaú<br>
        São  Paulo<br>
        RUA EDGAR FERRAZ, 1850<br>
        Antonio Aparecido Serra<br>
        (14)  99705-4122<br>
        antonioserra@uol.com.br</p>

    <p> Jundiaí<br>
        São  Paulo<br>
        R. APARECIDA CATOCCI LUCHINI, 43 APTO 43 BL. 01<br>
        Fernando  de Souza<br>
        (11) 99919-2798<br>
        ctmantovani2014@gmail.com</p>

    <p> Mauá<br>
        São  Paulo<br>
        RUA REGINALDO PEREIRA DA SILVA, 46 <br>
        Joelson Alves dos  Santos<br>
        (11) 97158-1512<br>
        jotao2000@hotmail.com</p>

    <p> Osasco<br>
        São  Paulo<br>
        RUA CONEGO AFONSO 254<br>
        Celso Antonio Giglio<br>
        (11)  99687-5959<br>
        sil_fdg@hotmail.com Silmara</p>

    <p> Pindamonhangaba<br>
        São Paulo<br>
        RUA CONÊGO TOBIAS 529<br>
        Rafael Goffi Moreira<br>
        (12)  99119-4551<br>
        secretaria@rafaelgoffi.com.br</p>

    <p> Poá<br>
        São  Paulo<br>
        RUA PADRE EUSTAQUIO, 490<br>
        Claudete Bezerra dos  Santos  Canada<br>
        (11) 99111-6300<br>
        claudete158@terra.com.br</p>

    <p> Praia Grande<br>
        São Paulo<br>
        RUA 25 DE JANEIRO Nº 89<br>
        Soraya de Andrade  Sartori<br>
        (13) 3495-7829<br>
        sorayasartori@bol.com.br</p>

    <p> Presidente  Prudente<br>
        São Paulo<br>
        RUA BELA 346<br>
        Wilson Portella  Rodrigues<br>
        (18) 3223-5376<br>
        wilsonportella@hotmail.com</p>

    <p> Ribeirão Pires<br>
        São Paulo<br>
        RUA XAVANTES, 54<br>
        Jose Cezarde Carvalho<br>
        (11)  99977-5135<br>
        advcezar@uol.com.br</p>

    <p> Santa Bárbara do  Oeste<br>
        São Paulo<br>
        RUA XV DE NOVEMBRO , 786 APTO. 01<br>
        Joel  Jose Pinto de Oliveira<br>
        (19) 99767-5150<br>
        joeljpoliveira1@gmail.com</p>

    <p> Santana de  Parnaíba<br>
        São Paulo<br>
        ALMEIDA SÃO CARLOS 343<br>
        Oswaldo Luiz  Oliveira Borrelli<br>
        (11) 4153-5236<br>
        oboreli@gmail.com</p>

    <p> Santo André<br>
        São Paulo<br>
        RUA ANTÔNIO BASTOS 210<br>
        Ricardo Ezequiel Torres<br>
        (11)  98141-4545<br>
        retorres45@gmail.com</p>

    <p> Santos<br>
        São  Paulo<br>
        AV. DR. EPITACIO PESSOA, 390 AP.113<br>
        Juan Manuel  Villarnobo Filho <br>
        (13) 99714-2205<br>
        secretaria@psdb45santos.com.br</p>

    <p> São Caetano do  Sul<br>
        São Paulo<br>
        RUA MARECHAL DEODORO 597, APT 161<br>
        Moacyr  Antonio Ferreira Rodrigues<br>
        (11)  4221-3151<br>
        moacyrrodrigues@uol.com.br</p>

    <p>São Paulo<br>
        São Paulo <br>
        Comitê Central PSDB  –Campanha do Aécio<br>
        Rua Bolívia, 398<br>
        (11)3894-4545<br>
        Horário de  funcionamento: dias de semana 8h às 20h, sábados 10h às 17h;  domingos até as 12h. </p>

    <p> Suzano<br>
        São  Paulo<br>
        RUA AURORA 146<br>
        Claudio Massamitsu Anzai<br>
        (11)  98187-9290<br>
        contato@claudiuanzai.com.br</p>


    <h2>REGIÃO  SUL</h2>
    <h3>PARANÁ</h3>

    <p>Almirante  Tamandaré<br>
        Paraná<br>
        AVENIDA EMILIO JOHNSON 174<br>
        Osvaldo  Estival<br>
        (41) 3657-2551<br>
        osvaldoestival@bol.com.br</p>

    <p>Campo  Largo<br>
        Paraná<br>
        RUA OSWALDO CRUZ 735<br>
        Udo Schimidt Neto<br>
        (41)  9703-3836<br>
        udosneto@yahoo.com.br</p>


    <p>Cambé<br>
        Paraná<br>
        RUA CELINO LIBONI 220<br>
        João Dalmacio Pavinatto<br>
        (43)  9917-5820<br>
        jpavinatto@gmail.com</p>

    <p>Foz do  Iguaçu<br>
        Paraná<br>
        AVENIDA PEDRO BASSO 72<br>
        Ivone Barofaldi da  Silva<br>
        (45) 3028-4545<br>
        ivone@calcepague.com.br</p>

    <p>Maringá<br>
        Paraná<br>
        Av.  PARANÁ ESQUINA COM A PRUDENTE DE MORAIS<br>
        Wilson de Matos Silva/  Marcelo<br>
        (44) 3027-6308<br>
        marcelo@unicesumar.edu.br</p>


    <p>Paranaguá<br>
        Paraná<br>
        RUA  ARTUR DE ABREU 29 - 1 ANDAR. CONJ 6<br>
        Alceu Maron Filho<br>
        (41)  9129-8910<br>
        alceuzinho@uol.com.br</p>


    <h3>RIO GRANDE DO  SUL</h3>

    <p>Alvorada<br>
        Rio  Grande do Sul<br>
        RUA JOAQUIM NABUCO, 298<br>
        Daniel Lobo de  Carvalh<br>
        (51) 9728-9514<br>
        danielcarvalho45@hotmail.com</p>

    <p>Bento Gonçalves<br>
        Rio  Grande do Sul<br>
        AV. OSVALDO ARANHA  1075  SALA 305<br>
        Orildes Maria  Lottici<br>
        (54) 9982-5412<br>
        orildeslottici@secdg.org.br</p>

    <p>Erechim<br>
        Rio  Grande do Sul<br>
        AV. TIRADENTES, 1619<br>
        Antonio Itacir Marcon<br>
        (54)  9998-3872<br>
        escritoriomarcon2012@hotmail.com</p>


    <p>Santa Maria<br>
        Rio  Grande do Sul<br>
        AV. MEDIANEIRA, 1547 / 501<br>
        Alexandre Gay de  Lima<br>
        (55) 9657-5375<br>
        diretoria@grupoapul.com.br</p>

    <p>São Leopoldo<br>
        Rio  Grande do Sul<br>
        RUA PEDRO PEREZ, 440<br>
        Valdir Farias de Mattos<br>
        (51)  9737-5494<br>
        valdirfmatos@gmail.com</p>
    <p>Viamão<br>
        Rio  Grande do Sul<br>
        AV. CORONEL MARCOS ANDRADE, 301<br>
        Valdir  Bonatto<br>
        (51) 9229-8775<br>
        prefeitobonatto@pmviamao.com.br</p>



    <p>Uruguaiana<br>
        Rio  Grande do Sul<br>
        AV. CAIRU, 393<br>
        José Francisco Sanchotene  Felice<br>
        (51) 9625-8193<br>
        sanchotenefelice@bol.com.br</p>


    <h3>SANTA CATARINA</h3>

    <p>Criciúma<br>
        Santa  Catarina<br>
        RODOVIA SC, 445 - KM 1<br>
        Clésio Salvaro<br>
        (48)  9158-4758 / 3045-4904<br>
        salvarocriciuma@gmail.com</p>
    <p>Joinville<br>
        Santa  Catarina<br>
        RUA SÃO FRANCISCO Nº 62<br>
        Erica K. Pereira<br>
        (47)  9988-2458<br>
        psdbjoinville@psdb45.org.br</p>


    <h2>REGIÃO  NORTE</h2>
    <h3>ACRE</h3>
    <p>Rio Branco<br>
        Acre<br>
        RUA  PIAUÍ, 246<br>
        José Benício Dias<br>
        (68)  9989-0477<br>
        j.beniciodias@hotmail.com</p>

    <h3>AMAZONAS</h3>
    <p>Manaus<br>
        Amazonas<br>
        RUA  RIO JACARÉ, 43 - CONJ. VIEIRA ALVES<br>
        Danisio Elias Souza<br>
        (92)  9141-7345<br>
        danisio45045@gmail.com</p>
    <h3>RONDÔNIA</h3>
    <p> Ji-Paraná<br>
        Rondônia<br>
        RUA ANGELIM 1831<br>
        Nair Barreto<br>
        (69)  9280-2324<br>
        nair.f.barreto@gmail.com </p>
    <h2>REGIÃO  NORDESTE</h2>
    <h3>BAHIA</h3>
    <p>Camaçari<br>
        Bahia<br>
        Rua  José Nunes de Matos, 233 (dentro da.casa do Forró)<br>
        Harrison  Freitas de Lima<br>
        (71) 9126-8731<br>
        harrisonfreitas@ibest.com.br</p>

    <p>Eunápolis<br>
        Bahia<br>
        Av.  Conselheiro Luiz Viana, 323<br>
        Valmirando Brito<br>
        (73)  9993-2494<br>
        valmirando45600@bol.com.br</p>

    <p>Feira de  Santana<br>
        Bahia<br>
        Rua Aloísio Rezende, 333<br>
        Sandro Ricardo  Lima<br>
        (75) 9115-4460<br>
        sandroricardolima@hotmail.com</p>

    <p>Itabuna<br>
        Bahia<br>
        Rua  Manoel Fogueira, 43<br>
        José Silva e Santos<br>
        (73)  9133-0493<br>
        zssceo@yahoo.com.br</p>

    <p>Jequié<br>
        Bahia<br>
        Rua  Francisco Carneiro, 134<br>
        Edelvito C. de Souza<br>
        (73)  9983-0376<br>
        ec.souza@uol.com.br</p>

    <p>Salvador<br>
        Bahia<br>
        AV.  TANCREDO NEVES, 274 - BL. A - SALA 212- CENTRO EMPRESARIAL  IGUATEMI<br>
        José Carlos F.da Silva<br>
        (71)  9968-5709<br>
        zecarlosfernandes45556@gmail.com</p>



    <h3>CEARÁ</h3>

    <p>Fortaleza<br>
        Ceará<br>
        RUA  MONSENHOR CATÃO, 947<br>
        Tomas P.P Filho<br>
        (85)  9998-9091<br>
        agenda.tomasff@gmail.com</p>

    <p>Maranguape<br>
        Ceará<br>
        Rua  Barbara de Alencar, 1810<br>
        Francisco Antonio Joca<br>
        (85)  9971-8830<br>
        franjoca@gmail.com</p>

    <h3>MARANHÃO</h3>

    <p>São  Luís<br>
        Maranhão<br>
        RUA DAS MACAÚBAS - Q. 51 - CASA 12 <br>
        José  Viana Junior<br>
        (61)&nbsp;9956-2045<br>
        jviana100@hotmail.com</p>

    <p> Timon<br>
        Maranhão<br>
        RUA 16, 1432<br>
        Daniel Coimbra<br>
        (86)  8841-5527<br>
        danielcoimbra@gmail.com</p>

    <h3>PARAÍBA</h3>

    <p>Campina  Grande<br>
        Paraíba<br>
        AV. PREFEITO SEVERINO BEZERRA CABRAL, 95<br>
        Jose  Marques Filho<br>
        (83)  8803-3232<br>
        jomafi@ig.com.br/josemarques.filho@uol.com.br</p>

    <p>Santa  Rita<br>
        Paraíba<br>
        RUA EMANUEL LISBOA DE LUCENA , S/N (referencia  granja pantanal)<br>
        Emerson Panta<br>
        emersonpanta@hotmail.com</p>

    <h3>PERNAMBUCO</h3>

    <p>Caruaru<br>
        Pernambuco<br>
        AV.  TANCREDO NEVES 278<br>
        Raffiê Dellon<br>
        (81)  9635-3051<br>
        raffiedellon@hotmail.com</p>

    <p>Jaboatão dos  Guararapes<br>
        Pernambuco<br>
        RUA GRANITO 762<br>
        Manoel Chavez<br>
        (81)  3476-2476/8803-4122<br>
        manoelchaves10@yahoo.com.br </p>
    <p>Petrolina<br>
        Pernambuco<br>
        AV.  DA INTEGRAÇÃO, 120<br>
        Guilherme Coelho<br>
        (87)  9998-0220<br>
        gcpetrolina@uol.com.br</p>


    <h3>SERGIPE </h3>
    <p>Aracaju<br>
        Sergipe<br>
        RUA  DUQUE DE CAXIAS, 326<br>
        Roberto Goes <br>
        (79)  9978-4878<br>
        psdb@infonet.com.br</p>

    <p>Lagarto<br>
        Sergipe<br>
        AV.  PRESIDENTE KENNEDY, 148 <br>
        José Wilame de Fraga<br>
        (79)  9966-3564<br>
        wilame.fraga@lagarto.se.gov.br</p>

</div>