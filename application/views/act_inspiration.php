<div id="blue-line">
    <a href="./<?php echo $this->uri->segment(1); ?>" class="title">Agir</a>
</div>
<div id="main">
    <h1>Inspiração</h1>
    <p> O que falta na sua comunidade? Que ações melhorariam a vida das pessoas ao seu redor? É hora de arregaçar as mangas e mudar o Brasil. Selecionamos algumas iniciativas para inspirar você. Escolha as mais interessantes, convoque seus amigos pelo Facebook e vamos agir! :) </p>
    <div class="link">
    <?php foreach($item as $key => $row):?>
        <a href="javascript:;" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php echo current_url();?>/<?php echo $key;?>/<?php echo $row->sanitize;?>', '<?php echo $row->title;?>', 'toolbar=0, status=0, width=650, height=450');">
            <p>
                <?php echo $row->description;?>
                <br>
                <input type="button" value="Convide seus amigos">
            </p>
            <img src="<?php echo $row->img;?>" height="205" width="310">
        </a>
    <?php endforeach; ?>
    </div>
</div>