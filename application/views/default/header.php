<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo $pageTitle; ?></title>

        <base href="<?php echo base_url(); ?>">
        <meta name="controller" content="<?php echo $this->router->class ?>" />
        <script src="./assets/js/jquery-1.11.0.min.js"></script>
        <link rel="shortcut icon" href="./assets/img/favicon.png" type="image/x-icon" />
        <?php echo isset($header) ? $header : NULL; ?>
        <?php echo isset($css) ? $css : NULL; ?>
        <?php echo isset($js) ? $js : NULL; ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div id="header">
            <div class="mob-menu"><span></span></div>
            <ul class="menu">
                <li>
                    <a href="./agir">
                        Agir
                    </a>
                </li>
                <li>
                    <a href="./compartilhe">
                        Compartilhe
                    </a>
                </li>
                <li>
                    <a href="./material-de-campanha">
                        Material de Campanha
                    </a>
                </li>
                <li>
                    <a href="./conheca">
                        Conheça
                    </a>
                </li>
                <li>
                    <a href="./legislacao">
                        Legislação
                    </a>
                </li>
            </ul>
            <a href="./" class="logo">
                <img src="./assets/img/logovamosagir.jpg" title="Página inicial" alt="Página inicial" width="169" height="73" />
            </a>
            <a href="http://www.aecioneves.com.br/" class="logo-aecio" target="_blank">
                <img src="./assets/img/logoaecio.jpg" title="Página inicial - Aécio" alt="Página inicial - Aécio" width="159" height="73" />
            </a>
        </div>
        <div id="yellow-line"></div>