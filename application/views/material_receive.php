<div id="blue-line">
    <a href="./<?php echo $this->uri->segment(1); ?>" class="title">Material de Campanha</a>
</div>
<div id="main">
    <h1>Receba em casa</h1>

    <p>Vamos mudar o Brasil, consiga o material da nossa campanha para espalhar nossas ideias.</p>
    <hr>
    <script type="text/javascript">var submitted=false;</script>
    <iframe name="hidden_iframe" id="hidden_iframe"
            style="display:none;" onload="if(submitted)
    {window.location='sucesso.php';}"></iframe>
    <form action="https://docs.google.com/forms/d/1C3JQIz_vpQFPcnHEq8OqDd5NP8GNJdx5IZhgbOjGkr0/formResponse" method="post"
          target="hidden_iframe" onsubmit="submitted=true;">
        <label for="name">
            Nome:*
        </label>
        <input type="text" name="entry.1234375307" id="name" aria-required="true" required="" />
        <label for="email">
            E-mail:*
        </label>
        <input type="email" name="entry.415270691" id="email" aria-required="true" required="" />
        <label for="cpf">
            CPF:*
            <span>O CPF é necessário para evitar perfis falsos e desperdício de material.</span>
        </label>
        <input type="text" name="entry.1631354896" id="cpf" aria-required="true" required="" />
        <label for="address">
            Endereço de Entrega:*
        </label>
        <input type="text" name="entry.996453611" id="address" aria-required="true" required="" />
        <label for="number">
            Número:*
        </label>
        <input type="text" name="entry.1732597481" id="number" aria-required="true" required="" />
        <label for="rg">
            Complemento:*
        </label>
        <input type="text" name="entry.610467164" id="complement aria-required="true" required="" />
        <label for="city">
            Cidade:*
        </label>
        <input type="text" name="entry.302209571" id="city aria-required="true" required="" />
        <label for="state">
            Estado:*
        </label>
        <select aria-required="true" required="" id="state" name="entry.1370177283">
            <option value="">Estado</option>
            <option value="AC">AC</option>
            <option value="AL">AL</option>
            <option value="AM">AM</option>
            <option value="AP">AP</option>
            <option value="BA">BA</option>
            <option value="CE">CE</option>
            <option value="DF">DF</option>
            <option value="ES">ES</option>
            <option value="GO">GO</option>
            <option value="MA">MA</option>
            <option value="MG">MG</option>
            <option value="MS">MS</option>
            <option value="MT">MT</option>
            <option value="PA">PA</option>
            <option value="PB">PB</option>
            <option value="PE">PE</option>
            <option value="PI">PI</option>
            <option value="PR">PR</option>
            <option value="RJ">RJ</option>
            <option value="RN">RN</option>
            <option value="RO">RO</option>
            <option value="RR">RR</option>
            <option value="RS">RS</option>
            <option value="SC">SC</option>
            <option value="SE">SE</option>
            <option value="SP">SP</option>
            <option value="TO">TO</option>
        </select>
        <label for="rg">
            CEP:*
        </label>
        <input type="text" name="entry.851927293" id="zipcode" aria-required="true" required="" />
        <div class="sticker">
            Pedido:*
            <span>Adesivos retangulares para carro. Quantidade: <input type="text" name="entry.40973686" aria-required="true" required="" /></span>
            <span>Adesivos redondos - bótons. Quantidade: <input type="text" name="entry.1731148130" aria-required="true" required="" /></span>
        </div>
        <label for="action">
            Descreva em poucas linhas como você vai<br> utilizar e distribuir esse material:
        </label>
        <textarea name="entry.2008126654" id="action"></textarea>
        <br />
        <input type="submit" value="Enviar">
    </form>
</div>