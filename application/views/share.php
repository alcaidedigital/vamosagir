<div id="blue-line">
    <a href="./<?php echo $this->uri->segment(1); ?>" class="title">Compartilhe</a>
</div>
<div id="main">
    <h1>O Brasil agindo</h1>
    <p> Participe ativamente na construção de um Brasil novo espalhando nossa mensagem.</p>
    <div class="link">
        <a href="./noticias">
            <img src="./assets/img/share/news.jpg" height="205" width="310" class="scale-1-7">
        </a>
        <a href="./<?php echo $this->uri->segment(1)?>/brasil-agindo">
            <img src="./assets/img/share/brasil.jpg" height="205" width="310" class="scale-1-7">
        </a>
        <a href="./documentos">
            <img src="./assets/img/share/doc.jpg" height="205" width="310" class="scale-1-7">
        </a>
        <a href="https://twitter.com/AecioNeves" target="_blank">
            <img src="./assets/img/share/twitter.jpg" height="205" width="310" class="scale-1-7">
        </a>
        <a href="http://instagram.com/aecionevesoficial" target="_blank">
            <img src="./assets/img/share/instagram.jpg" height="205" width="310" class="scale-1-7">
        </a>
        <a href="https://www.facebook.com/AecioNevesOficial" target="_blank">
            <img src="./assets/img/share/facebook.jpg" height="205" width="310" class="scale-1-7">
        </a>
    </div>
</div>