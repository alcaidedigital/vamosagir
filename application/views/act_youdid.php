<div id="blue-line">
    <a href="./<?php echo $this->uri->segment(1); ?>" class="title">Agir</a>
</div>
<div id="main">
    <h1>Conte o que você fez</h1>

    <p>Existe um país diferente nascendo pelas ações de milhares de brasileiros. Conte pra gente sobre o seu projeto e vamos construir um novo Brasil. </p>
    <hr>
    <form action="<?php echo $this->router->class; ?>/sendyoudid" method="post" enctype="multipart/form-data" class="form">
        <label for="name">
            Nome:*
        </label>
        <input type="text" name="name" id="name" aria-required="true" required="" />
        <label for="city">
            Cidade:*
        </label>
        <input type="text" name="city" id="city" aria-required="true" required="" />
        <label for="zipcode">
            CEP:*
        </label>
        <input type="text" name="zipcode" id="zipcode" aria-required="true" required="" />
        <label for="email">
            E-mail:*
        </label>
        <input type="email" name="email" id="email" aria-required="true" required="" />
        <label for="file">
            Arquivo:*
            <span>Faça o upload de foto ou video da sua ação</span>
        </label>
        <input type="file" name="file" id="file" />
        <label for="action">
            Ação e resultados: *
            <span>Quantas pessoas foram mobilizadas? Qual foi o benefício? Conte tudo pra gente.</span>
        </label>
        <textarea name="action" id="action" cols="30" rows="10" aria-required="true" required="" /></textarea>
        <input type="submit" value="Enviar">
    </form>
</div>