<?php include 'default/header.php'; ?>
<div id="sign-in">
    <script type="text/javascript">var submitted=false;</script>
    <iframe name="hidden_iframe" id="hidden_iframe"
            style="display:none;" onload="if(submitted)
    {window.location='sucesso.php';}"></iframe>
    <form action="https://docs.google.com/forms/d/1vSBSRGQVho7YFqOWBY9nYumv940qB86wPmzw_ntu0Q0/formResponse" method="post"
          target="hidden_iframe" onsubmit="submitted=true;">
        <img src="./assets/img/vemfazerdiferente.png" width="520" height="54"/>
        <label for="email" ">Email:</label>
        <input type="email" aria-required="true" aria-label="Email:" id="email" required="" value="" name="entry.1369837723">
        <select aria-required="true" required="" id="state" name="entry.448085040">
            <option value="">Estado</option>
            <option value="AC">AC</option>
            <option value="AL">AL</option>
            <option value="AM">AM</option>
            <option value="AP">AP</option>
            <option value="BA">BA</option>
            <option value="CE">CE</option>
            <option value="DF">DF</option>
            <option value="ES">ES</option>
            <option value="GO">GO</option>
            <option value="MA">MA</option>
            <option value="MG">MG</option>
            <option value="MS">MS</option>
            <option value="MT">MT</option>
            <option value="PA">PA</option>
            <option value="PB">PB</option>
            <option value="PE">PE</option>
            <option value="PI">PI</option>
            <option value="PR">PR</option>
            <option value="RJ">RJ</option>
            <option value="RN">RN</option>
            <option value="RO">RO</option>
            <option value="RR">RR</option>
            <option value="RS">RS</option>
            <option value="SC">SC</option>
            <option value="SE">SE</option>
            <option value="SP">SP</option>
            <option value="TO">TO</option>
        </select>
        <input type="submit" value="Faça parte"/>
    </form>
</div>
<div id="banner">
    <ul class="pagination"></ul>
    <a href="" class="link"></a>
</div>