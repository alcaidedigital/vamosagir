<div id="blue-line">
    <div class="menu">
        <a href="./agir" class="agir">
            <img src="./assets/img/btn_agir.png" height="54" width="164" />
            <img src="./assets/img/btn_agir_hover.png" height="54" width="164" class="hover" />
        </a>
        <a href="./compartilhe" class="compartilhe">
            <img src="./assets/img/btn_compartilhe.png" height="54" width="211" />
            <img src="./assets/img/btn_compartilhe_hover.png" height="54" width="211" class="hover" />
        </a>
        <a href="./material-de-campanha" class="material">
            <img src="./assets/img/btn_material.png" height="54" width="322" />
            <img src="./assets/img/btn_material_hover.png" height="54" width="322" class="hover" />
        </a>
    </div>
</div>
<div id="main">
    <h1>É hora de mudar o Brasil e precisamos de você.</h1>
    <p class="subtitle">Queremos um país melhor e, com a ajuda de todos, nós vamos conseguir.<br>
        Crie projetos para a sua comunidade ou participe das propostas que surgirem por aqui.</p>
    <hr>
    <ul class="news">
    <?php foreach($news as $row): ?>
        <li>
            <a href="<?php echo $row->link; ?>">
                <p class="title"><?php echo $row->title; ?></p>
                <p class="text"><?php echo $row->text; ?></p>
                <img src="<?php echo $row->thumb; ?>" alt="<?php echo $row->title; ?>" title="<?php echo $row->title; ?>" />
            </a>
        </li>
    <?php endforeach ?>
    </ul>
    <div style="clear: both"></div>
    <a href="./noticias" class="read-more"> Mais notícias</a>
</div>