<div id="blue-line">
    <a href="./<?php echo $this->uri->segment(1); ?>" class="title">Documentos</a>
</div>
<div id="main">
    <div class="sidebar">
        <h3>VERSÃO PARA IMPRESSÃO</h3>
        <a href="<?php echo $document->file; ?>" target="_blank" title="">
            <img src="./assets/img/pdf.jpg" width="150" height="110"/>
        </a>
    </div>
    <div class="article">
        <div class="info-header">
            <div class="local-time"><?php echo $document->date; ?> | <?php echo $document->local; ?></div>
            <div class="share">
                <span class="text">Compartilhar</span>
                <a href="javascript: void(0);" onClick="window.open('http://www.facebook.com/sharer.php?u=<?php echo current_url();?>','Share', 'toolbar=0, status=0, width=650, height=450');">
                    <img src="./assets/img/socialicon/fb.png" height="22" width="22">
                </a>
                <a href="javascript: void(0);" onClick="window.open('http://twitter.com/intent/tweet?source=sharethiscom&url=<?php echo current_url();?>','Share', 'toolbar=0, status=0, width=650, height=450');">
                    <img src="./assets/img/socialicon/twitter.png" height="22" width="22">
                </a>
                <a href="javascript: void(0);" onClick="window.open('https://plus.google.com/share?url=<?php echo current_url();?>','Share', 'toolbar=0, status=0, width=650, height=450');">
                    <img src="./assets/img/socialicon/plus.png" height="22" width="22">
                </a>
            </div>
            <div style="clear: both"></div>
        </div>
        <h2><?php echo $document->title; ?></h2>
        <div class="text">
            <?php echo $document->text; ?>
        </div>
    </div>
</div>