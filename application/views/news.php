<div id="blue-line">
    <a href="./<?php echo $this->uri->segment(1); ?>" class="title">Notícias</a>
</div>
<div id="main">
    <ul class="list">
        <?php foreach ($news as $row): ?>
            <li>
                <a href="<?php echo $row->link ?>">
                    <p class="title"><?php echo $row->title ?></p>
                    <img src="<?php echo $row->thumb; ?>" title="<?php echo $row->title ?>"
                         alt="<?php echo $row->title ?>"/>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>