<div id="blue-line">
    <a href="./<?php echo $this->uri->segment(1); ?>" class="title">Agir</a>
</div>
<div id="main">
    <h1>Faça Acontecer</h1>
    <p>É hora de colocar suas ideias em prática. Crie projetos, pense em soluções para sua comunidade e vamos agir. Convide amigos em suas redes sociais e fale com todo mundo que pode ajudar. O Brasil quer mudar e precisa de você.</p>
    <div class="link">
        <a href="./<?php echo $this->uri->segment(1)?>/inspiracao">
            <img src="./assets/img/act/inspiracao.jpg" height="205" width="310" class="scale-1-7">
        </a>
        <a href="./<?php echo $this->uri->segment(1)?>/o-que-estamos-fazendo">
            <img src="./assets/img/act/oqueestamos.jpg" height="205" width="310" class="scale-1-7">
        </a>
        <a href="./<?php echo $this->uri->segment(1)?>/conte-o-que-voce-fez">
            <img src="./assets/img/act/conteoquevcfez.jpg" height="205" width="310" class="scale-1-7">
        </a>
    </div>
</div>