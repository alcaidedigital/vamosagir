<div id="blue-line">
    <a href="./<?php echo $this->uri->segment(1); ?>" class="title">Material de Campanha</a>
</div>
<div id="main">
    <h1>Imprima</h1>
    <p>Imprima o material de campanha em casa. Aqui, você encontra todos os arquivos que precisa.</p>

    <hr>
    <p><strong>Baixe as fotos de Aécio em alta resolução</strong></p>
    <img src="./assets/img/material/printout/aecio.png" height="409" width="997" />
    <input type="button" value="Baixar Imagens" class="photo">


    <hr>
    <p><strong>Baixe os bottons em alta resolução e imprima</strong></p>
    <img src="./assets/img/material/printout/bottons.png" height="461" width="856" />
    <input type="button" value="Baixar Material" class="botton">


    <hr>
    <p><strong>Baixe adesivos oficiais em qualidade alta</strong></p>
    <img src="./assets/img/material/printout/sticker.png" height="375" width="775" />
    <input type="button" value="Baixar Material" class="sticker">


    <hr>
    <p><strong>Baixe as bandeiras oficiais em qualidade alta </strong></p>
    <img src="./assets/img/material/printout/flags.png" height="186" width="888" />
    <input type="button" value="Baixar Material" class="flag">


</div>