<div id="blue-line">
    <a href="./<?php echo $this->uri->segment(1); ?>" class="title">Notícias</a>
</div>
<div id="main">
    <div class="sidebar">
        <?php if(count($news->img) > 0) :?>
            <h3>Fotos</h3>
            <?php foreach ($news->img as $row) :?>
                <a class="fancybox" href="<?php echo $row->src; ?>" data-fancybox-group="gallery" title=""><img src="<?php echo $row->src; ?>" alt="" /></a>
            <?php endforeach; ?>
        <?php endif; ?>
        <?php if(count($news->youtube) > 0) :?>
            <h3>Vídeos</h3>
            <?php foreach ($news->youtube as $row) :?>
                <a class="video" href="<?php echo $row['url']; ?>"" data-fancybox-group="gallery" title="">
                    <img src="./assets/img/play.png" alt="" class="icon" />
                    <img src="<?php echo $row['img']['small']; ?>" alt="" />
                </a>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <div class="article">
        <div class="info-header">
            <div class="local-time"><?php echo $news->date; ?> | <?php echo $news->local; ?></div>
            <div class="share">
                <span class="text">Compartilhar</span>
                <a href="javascript: void(0);" onClick="window.open('http://www.facebook.com/sharer.php?u=<?php echo current_url();?>','Share', 'toolbar=0, status=0, width=650, height=450');">
                    <img src="./assets/img/socialicon/fb.png" height="22" width="22">
                </a>
                <a href="javascript: void(0);" onClick="window.open('http://twitter.com/intent/tweet?source=sharethiscom&url=<?php echo current_url();?>','Share', 'toolbar=0, status=0, width=650, height=450');">
                    <img src="./assets/img/socialicon/twitter.png" height="22" width="22">
                </a>
                <a href="javascript: void(0);" onClick="window.open('https://plus.google.com/share?url=<?php echo current_url();?>','Share', 'toolbar=0, status=0, width=650, height=450');">
                    <img src="./assets/img/socialicon/plus.png" height="22" width="22">
                </a>
            </div>
            <div style="clear: both"></div>
        </div>
        <h2><?php echo $news->title; ?></h2>
        <div class="text">
            <?php echo $news->text; ?>
        </div>
    </div>
</div>