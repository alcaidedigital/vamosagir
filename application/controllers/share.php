<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class share
 */
class Share extends My_Controller
{
    public function index()
    {
        $this->setPageTitle('Compartilhe');
        parent::renderer();
    }

    public function acting()
    {
        $this->setPageTitle('Brasil Agindo');
        $this->content = $this->router->class . '_' . $this->router->method;
        $this->data['css'] .= loadCss($this->content);
        $this->data['header'] .= loadFancybox();

        $this->data['img'] = array();
        $this->data['img'][] = './assets/img/share/acting/44444444.png';
        $this->data['img'][] = './assets/img/share/acting/aecio_austin.jpg';
        $this->data['img'][] = './assets/img/share/acting/belohorizonte.jpg';
        $this->data['img'][] = './assets/img/share/acting/lica.jpg';
        $this->data['img'][] = './assets/img/share/acting/marapendi.jpg';
        $this->data['img'][] = './assets/img/share/acting/taguai.jpg';
        $this->data['img'][] = './assets/img/share/acting/adesivo-pca-general-osorio.jpg';
        $this->data['img'][] = './assets/img/share/acting/copacabana-1.jpg';
        $this->data['img'][] = './assets/img/share/acting/ECOBALSAS.jpg';
        $this->data['img'][] = './assets/img/share/acting/padre_miguel.jpg';
        $this->data['img'][] = './assets/img/share/acting/Bauru.jpg';
        $this->data['img'][] = './assets/img/share/acting/RIO-PRETO.jpg';
        $this->data['img'][] = './assets/img/share/acting/Arte_Franca.png';

        $this->generateMeta($this->data['img']);

        parent::renderer();
    }
    private function generateMeta($img)
    {
        $this->load->helper('html');
        //$this->data['header'] .= meta('fb:app_id', '370117756342202');
        $this->data['header'] .= meta('og:locale', 'pt_BR');
        $this->data['header'] .= meta('og:type', 'article');
        $this->data['header'] .= meta('og:description', 'Muita gente já se juntou para a mudança acontecer. Estamos por todos os lados em busca de um país melhor. Confira o que já rolou e #vamosagir');
        foreach($img as $row) {
            $this->data['header'] .= meta('og:image', base_url(str_replace('./assets', 'assets', $row)));
        }
    }

}

/* End of file share.php */
/* Location: ./application/controllers/share.php */