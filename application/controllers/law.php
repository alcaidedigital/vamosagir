<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class law
 */
class Law extends My_Controller
{
    public function index()
    {
        $this->setPageTitle('Legislação');
        parent::renderer();
    }
}

/* End of file law.php */
/* Location: ./application/controllers/law.php */