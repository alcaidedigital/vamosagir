<?php
class Receive extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('receive_model');
    }
    public function index(){
        $this->data['list'] = $this->receive_model->get()->result();
        $this->data['header'] = loadDataTable();
        parent::index();
    }
    public function edit($id = NULL){
        if($this->uri->segment(3) == 'editar'
            && (int) $id === 0) {
            redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/novo');
        }else{
            $data = $this->receive_model->get(array('id' => $id))->result();
            $data = current($data);
            $this->data['data'] = $data;
        }
        $this->data['header'] .= loadValidator() . loadMask();
        $this->content = $this->uri->segment(1) . '/' . $this->router->class . '_edit';
        parent::index();
    }
    public function delete($id){
        $this->receive_model->delete(array('id' => $id));
        $this->setMsg('Registro excluido com sucesso');
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2));
    }
}

/* End of file receive.php */
/* Location: ./application/controllers/adm/receive.php */