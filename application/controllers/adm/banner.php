<?php
class Banner extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Banner_model');
    }
    public function index(){
        $this->data['list'] = $this->Banner_model->get()->result();
        $this->data['header'] = loadDataTable();
        parent::index();
    }
    public function edit($id = NULL){
        if($this->uri->segment(3) == 'editar'
            && (int) $id === 0) {
            redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/novo');
        }else{
            $data = $this->Banner_model->get(array('id' => $id))->result();
            $data = current($data);
            $this->data['data'] = $data;
        }
        $this->data['header'] .= loadValidator() . loadMask();
        $this->content = $this->uri->segment(1) . '/' . $this->router->class . '_edit';
        parent::index();
    }
    public function record($id = NULL){
        $id = (int)$id;
        if($this->input->post()){
            $data = $this->input->post();

            if($_FILES['img']['size'] > 0) {

                if($id !== 0){
                    $tempVar = $this->Banner_model->get(array('id' => $id))->result();
                    $tempVar = current($tempVar);
                    @unlink($tempVar->img);
                }
                $uploadPath = './assets/uploads/' . $this->router->class . '/';

                if (!is_dir($uploadPath)) {
                    mkdir($uploadPath, FILE_WRITE_MODE, TRUE);
                }

                $this->load->library('upload', array('upload_path' => $uploadPath, 'allowed_types' => 'gif|jpg|png'));

                if ( ! $this->upload->do_upload('img')) {
                    $this->setError($this->upload->display_errors());
                }else{
                    $file = $this->upload->data();

                    $this->load->library('image_lib', array(
                        'width' => 1559,
                        'height' => 333,
                        'source_image'  => $file['full_path'],
                        'quality' => '100%'
                    ));

                    if ( ! $this->image_lib->resize()){
                        $this->setError($this->image_lib->display_errors());
                    }
                    $data['img'] = $uploadPath . $file['file_name'];
                    chmod($data['img'], FILE_READ_MODE);
                }
            }
            if($id === 0){
                $this->Banner_model->insert($data);
            }else{
                $this->Banner_model->update(array('id' => $id), $data);
            }
            $this->setMsg('Registro atualizado com sucesso');
        }else{
            $this->setError('Ocorreu um erro ao processar o formulario, tente novamente mais tarde');
        }
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2));
    }
    public function delete($id){
        $data = $this->Banner_model->get(array('id' => $id))->result();
        $data = current($data);
        @unlink($data->img);
        $this->Banner_model->delete(array('id' => $id));
        $this->setMsg('Registro excluido com sucesso');
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2));
    }
}

/* End of file Banner.php */
/* Location: ./application/controllers/adm/Banner.php */