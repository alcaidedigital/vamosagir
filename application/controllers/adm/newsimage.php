<?php
class Newsimage extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('newsimage_model');
    }
    public function index($id){
        $this->data['id'] = $id;
        $this->data['list'] = $this->newsimage_model->get(array('news_id' => $id))->result();
        parent::renderer();
    }
    public function record($id){
        if(isset($_FILES['src'])) {
            $this->load->model('news_model');

            $news = $this->news_model->get(array('id' => $id))->result();
            $news = current($news);

            $data = array('news_id' => $id);
            $uploadPath = './assets/uploads/' . $this->router->class . '/';

            if (!is_dir($uploadPath)) {
                mkdir($uploadPath, 0777, TRUE);
            }
            $uploadPath .= $news->id . '/';
            if (!is_dir($uploadPath)) {
                mkdir($uploadPath, 0777, TRUE);
            }

            $this->load->library('upload', array('upload_path' => $uploadPath, 'allowed_types' => 'gif|jpg|png'));

            $name = md5(date('r'));
            foreach($_FILES['src']['name'] as $key => $row) {
                $_FILES[$name]['name'] = $_FILES['src']['name'][$key];
                $_FILES[$name]['type'] = $_FILES['src']['type'][$key];
                $_FILES[$name]['tmp_name'] = $_FILES['src']['tmp_name'][$key];
                $_FILES[$name]['error'] = $_FILES['src']['error'][$key];
                $_FILES[$name]['size'] = $_FILES['src']['size'][$key];

                if ( ! $this->upload->do_upload($name)) {
                    $this->setError($this->upload->display_errors());
                }else{
                    $file = $this->upload->data();
                    $data['src'] = $uploadPath . $file['file_name'];
                    $this->newsimage_model->insert($data);
                    chmod($data['src'], FILE_READ_MODE);
                }
            }
        }else{
            $data = $this->input->post('id');
            foreach($data as $key => $row) {
                if((int)$row > 0) {
                    $this->newsimage_model->update(array('id' => $key), array('order' => $row));
                }
            }
        }
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' . $id);
    }
    public function delete($news_id, $id){
        $data = $this->newsimage_model->get(array('id' => $id))->result();
        $data = current($data);
        @unlink($data->src);
        $this->newsimage_model->delete(array('id' => $id));
        $this->setMsg('Registro excluido com sucesso');
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' . $news_id);
    }
}

/* End of file newsimage.php */
/* Location: ./application/controllers/adm/newsimage.php */