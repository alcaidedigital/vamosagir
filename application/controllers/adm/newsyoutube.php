<?php
class Newsyoutube extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('newsyoutube_model');
    }
    public function index($id){
        $this->data['id'] = $id;
        $this->data['list'] = $this->newsyoutube_model->get(array('news_id' => $id))->result();
        $this->load->library('youtube');
        foreach($this->data['list'] as $key => $row) {
            if(strlen($row->url) > 0) {
                $row->data = $this->youtube->getInfor($row->url);
                $this->data['list'][$key] = $row;
            }
        }
        parent::renderer();
    }
    public function record($id){
        $url = $this->input->post('url');
        $data = array(
            'news_id' => $id,
            'order' => 0
        );
        foreach($url as $row) {
            if(strlen($row) > 0) {
                $data['url'] = $row;
                $this->newsyoutube_model->insert($data);
            }
        }
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' . $id);
    }
    public function delete($news_id, $id){
        $this->newsyoutube_model->delete(array('id' => $id));
        $this->setMsg('Registro excluido com sucesso');
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' . $news_id);
    }
}

/* End of file newsyoutube.php */
/* Location: ./application/controllers/adm/newsyoutube.php */