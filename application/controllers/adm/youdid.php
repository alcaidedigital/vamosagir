<?php
class Youdid extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('youdid_model');
    }
    public function index(){
        $this->data['list'] = $this->youdid_model->get()->result();
        $this->data['header'] = loadDataTable();
        parent::index();
    }
    public function edit($id = NULL){
        if($this->uri->segment(3) == 'editar'
            && (int) $id === 0) {
            redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/novo');
        }else{
            $data = $this->youdid_model->get(array('id' => $id))->result();
            $data = current($data);
            $this->data['data'] = $data;
        }
        $this->data['header'] .= loadValidator() . loadMask();
        $this->content = $this->uri->segment(1) . '/' . $this->router->class . '_edit';
        parent::index();
    }
    public function delete($id){
        $this->youdid_model->delete(array('id' => $id));
        $this->setMsg('Registro excluido com sucesso');
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2));
    }
    public function excel()
    {
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $c = 'A';
        $l = 1;
        $this->excel->getActiveSheet()->setTitle('__');
        $this->excel->getActiveSheet()->SetCellValue($c++ . $l, 'Nome');
        $this->excel->getActiveSheet()->SetCellValue($c++ . $l, 'Cidade');
        $this->excel->getActiveSheet()->SetCellValue($c++ . $l, 'CEP');
        $this->excel->getActiveSheet()->SetCellValue($c++ . $l, 'E-mail');
        $this->excel->getActiveSheet()->SetCellValue($c++ . $l, 'Arquivo');
        $this->excel->getActiveSheet()->SetCellValue($c++ . $l, 'Ação');

        $result = $this->youdid_model->get()->result();

        foreach ($result as $row) {
            $c = 'A';
            $l++;
            $this->excel->getActiveSheet()->SetCellValue($c++ . $l, $row->name);
            $this->excel->getActiveSheet()->SetCellValue($c++ . $l, $row->city);
            $this->excel->getActiveSheet()->SetCellValue($c++ . $l, $row->zipcode);
            $this->excel->getActiveSheet()->SetCellValue($c++ . $l, $row->email);
            $this->excel->getActiveSheet()->SetCellValue($c++ . $l, $row->file ? base_url($row->file) : NULL);
            $this->excel->getActiveSheet()->SetCellValue($c++ . $l, $row->action);
        }
        $filename = date('dmY_His') .  '.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }
}

/* End of file youdid.php */
/* Location: ./application/controllers/adm/youdid.php */