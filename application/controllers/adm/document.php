<?php
class Document extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('document_model');
    }
    public function index(){
        $this->data['list'] = $this->document_model->get()->result();
        foreach($this->data['list'] as $key => $row) {
            $row->date = convertDate($row->date);
            $this->data['list'][$key] = $row;
        }
        $this->data['header'] = loadDataTable();
        parent::renderer();
    }
    public function edit($id = NULL){
        if($this->uri->segment(3) == 'editar'
            && (int) $id === 0) {
            redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/novo');
        }else{
            $data = $this->document_model->get(array('id' => $id))->result();
            if(count($data) > 0) {
                $data = current($data);
                $data->date = convertDate($data->date);
                $this->data['data'] = $data;
            }
        }
        $this->data['header'] .= loadValidator() . loadMask() . loadCkeditor();
        $this->content = $this->uri->segment(1) . '/' . $this->router->class . '_edit';
        parent::index();
    }
    public function record($id = NULL){
        $id = (int)$id;
        if($this->input->post()){
            $this->form_validation->set_rules('title', 'Título', 'trim|required|min_length[3]');

            if($this->form_validation->run() === FALSE){
                $this->setError(validation_errors());
                if($id === 0){
                    $redirect = '/novo';
                }else{
                    $redirect = '/editar/' . $id;
                }
                redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) .  $redirect);
            } else {
                $data = $this->input->post();
                $data['date'] = convertDate($data['date']);
                $uploadPath = './assets/uploads/' . $this->router->class . '/';

                if (!is_dir($uploadPath)) {
                    mkdir($uploadPath, 0777, TRUE);
                }

                if($_FILES['thumb']['size'] > 0) {
                    if($id !== 0){
                        $tempVar = $this->document_model->get(array('id' => $id))->result();
                        $tempVar = current($tempVar);
                        @unlink($tempVar->thumb);
                    }
                    $this->load->library('upload', array('upload_path' => $uploadPath, 'allowed_types' => 'gif|jpg|png'));

                    if ( ! $this->upload->do_upload('thumb')) {
                        $this->setError($this->upload->display_errors());
                    }else{
                        $file = $this->upload->data();

                        $this->load->library('image_lib', array(
                            'width' => 300,
                            'height' => 153,
                            'maintain_ratio' => true,
                            'source_image'  => $file['full_path'],
                            'quality' => '80%'
                        ));

                        if ( ! $this->image_lib->resize()){
                            $this->setError($this->image_lib->display_errors());
                        }
                        $data['thumb'] = $uploadPath . $file['file_name'];
                    }
                }

                if($_FILES['file']['size'] > 0) {
                    if($id !== 0){
                        $tempVar = $this->document_model->get(array('id' => $id))->result();
                        $tempVar = current($tempVar);
                        @unlink($tempVar->file);
                    }
                    $this->upload->set_allowed_types('pdf|doc|docx');

                    if ( ! $this->upload->do_upload('file')) {
                        $this->setError($this->upload->display_errors());
                    }else{
                        $file = $this->upload->data();

                        $data['file'] = $uploadPath . $file['file_name'];
                        chmod($data['file'], FILE_READ_MODE);
                    }
                }
                if($id === 0){
                    $this->document_model->insert($data);
                }else{
                    $this->document_model->update(array('id' => $id), $data);
                }
                $this->setMsg('Registro atualizado com sucesso');
            }
        }else{
            $this->setError('Ocorreu um erro ao processar o formulario, tente novamente mais tarde');
        }
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2));
    }
    public function delete($id){
        $data = $this->document_model->get(array('id' => $id))->result();
        $data = current($data);
        @unlink($data->thumb);
        @unlink($data->file);
        $this->document_model->delete(array('id' => $id));
        $this->setMsg('Registro excluido com sucesso');
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2));
    }
}

/* End of file document.php */
/* Location: ./application/controllers/adm/document.php */