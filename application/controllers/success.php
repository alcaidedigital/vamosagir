<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Success
 */
class Success extends My_Controller
{
    public function index()
    {
        parent::renderer();
    }
}

/* End of file success.php */
/* Location: ./application/controllers/success.php */