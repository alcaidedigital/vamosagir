<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class material
 */
class Material extends My_Controller
{
    public function index()
    {
        $this->setPageTitle('Material de Campanha');
        parent::renderer();
    }
    public function places()
    {
        $this->setPageTitle('Locais para retirada');
        $this->content = $this->router->class . '_' . $this->router->method;
        parent::renderer();
    }
    public function printout()
    {
        $this->setPageTitle('Imprima');
        $this->content = $this->router->class . '_' . $this->router->method;
        $this->data['css'] .= loadCss($this->content);
        $this->data['js'] .= loadJs($this->content);
        parent::renderer();
    }
    public function receive()
    {
        $this->setPageTitle('Receba em casa');
        $this->content = $this->router->class . '_' . $this->router->method;
        $this->data['header'] .= loadMask();
        $this->data['css'] .= loadCss($this->content);
        $this->data['js'] .= loadJs($this->content);
        parent::renderer();
    }
    public function bedistributor()
    {
        $this->setPageTitle('Seja um distribuidor');
        $this->data['header'] .= loadMask();
        $this->content = $this->router->class . '_' . $this->router->method;
        $this->data['css'] .= loadCss($this->content);
        $this->data['js'] .= loadJs($this->content);
        parent::renderer();
    }
    public function sendbedistributor()
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Nome', 'trim|required|min_length[3]');
            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
            $this->form_validation->set_rules('rg', 'RG', 'trim|required|min_length[9]');

            if ($this->form_validation->run() === FALSE) {
                $this->setError(validation_errors());
            } else {
                $this->load->model('bedistributor_model');
                $data = $this->input->post();
                $this->bedistributor_model->insert($data, true);
            }
        } else {
            $this->setError('Ocorreu um erro ao processar o formulario, tente novamente mais tarde');
        }
        redirect(base_url('sucesso'));
    }
    public function sendreceive()
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Nome', 'trim|required|min_length[3]');
            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');

            if ($this->form_validation->run() === FALSE) {
                $this->setError(validation_errors());
            } else {
                $this->load->model('receive_model');
                $data = $this->input->post();
                $this->receive_model->insert($data, true);
            }
        } else {
            $this->setError('Ocorreu um erro ao processar o formulario, tente novamente mais tarde');
        }
        redirect(base_url('sucesso'));
    }

}

/* End of file material.php */
/* Location: ./application/controllers/material.php */