<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Home
 */
class Home extends My_Controller
{
    public function index()
    {
        $this->header = 'headerhome';
        $this->data['js'] .= loadJs('banner');
        $this->data['css'] .= loadCss('banner');
/*
        $this->load->model('banner_model');
        $banner = $this->banner_model->get()->result();
*/
        $banner = array();
        $banner[] = array(
            'img' => './assets/img/banner/cover_virada.jpg',
            'link' => 'http://www.vamosagir45.com.br/'
        );
        /*
        $banner[] = array(
            'img' => './assets/img/banner/cover_aecio_lancamento.jpg',
            'link' => 'noticias/21/lancamento-oficial-do-site-vamos-agir'
        );
        $banner[] = array(
            'img' => './assets/img/banner/cover_aecio.jpg',
            'link' => ''
        );
        $banner[] = array(
            'img' => './assets/img/banner/cover_oquefaz.jpg',
            'link' => 'agir/conte-o-que-voce-fez'
        );
        $banner[] = array(
            'img' => './assets/img/banner/cover_FHC.jpg',
            'link' => 'noticias/5/fhc-fala-para-os-voluntarios'
        );
        $banner[] = array(
            'img' => './assets/img/banner/cover_aecio_pessoas.jpg',
            'link' => 'noticias'
        );
        */
        $this->data['header'] .= '<script>jQuery(function () {setBackground(' . json_encode($banner) . ');});</script>';

        $this->load->model('news_model');
        $this->data['news'] = $this->news_model->featured()->result();

        foreach($this->data['news'] as $key => $row) {
            $row->sanitize = url_title(convert_accented_characters($row->title), '-', true);
            $row->link = './noticias/' . $row->id . '/' . $row->sanitize;


            $row->text = substr($row->text,0, strpos($row->text, "</p>")+4);
            $row->text = strip_tags($row->text);

            $this->data['news'][$key] = $row;
        }

        parent::renderer();
    }

    public function sendbepart()
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
            $this->form_validation->set_rules('state', 'Estado', 'trim|required');

            if ($this->form_validation->run() === FALSE) {
                $this->setError(validation_errors());
            } else {
                $this->load->model('bepart_model');
                $data = $this->input->post();
                $this->bepart_model->insert($data, true);
            }
        } else {
            $this->setError('Ocorreu um erro ao processar o formulario, tente novamente mais tarde');
        }
        redirect(base_url('sucesso'));
    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */