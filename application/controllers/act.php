<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Act
 */
class Act extends My_Controller
{
    public function index()
    {
        $this->setPageTitle('Agir');
        parent::renderer();
    }

    public function inspiration($id, $title)
    {
        $this->setPageTitle('Inspiração');
        $this->content = $this->router->class . '_' . $this->router->method;
        $this->data['css'] .= loadCss($this->content);

        $this->data['item'] = $this->generateItem();
        $this->generateMeta($id);

        parent::renderer();
    }

    public function whatwearedoing()
    {
        $this->setPageTitle('O que estamos fazendo');
        $this->content = $this->router->class . '_' . $this->router->method;

        parent::renderer();
    }

    public function youdid()
    {
        $this->setPageTitle('Conte o que você fez');
        $this->data['header'] .= loadMask();
        $this->content = $this->router->class . '_' . $this->router->method;
        $this->data['css'] .= loadCss($this->content);
        $this->data['js'] .= loadJs($this->content);

        parent::renderer();
    }

    public function sendyoudid()
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Nome', 'trim|required|min_length[3]');
            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
            $this->form_validation->set_rules('zipcode', 'CEP', 'trim|required|min_length[9]');

            if ($this->form_validation->run() === FALSE) {
                $this->setError(validation_errors());
            } else {
                $this->load->model('youdid_model');
                $data = $this->input->post();

                if($_FILES['file']['size'] > 0) {
                    $uploadPath = './assets/uploads/' . $this->router->class . '/';

                    if (!is_dir($uploadPath)) {
                        mkdir($uploadPath, 0777, TRUE);
                    }

                    $this->load->library('upload', array('upload_path' => $uploadPath, 'allowed_types' => '*'));

                    if ( ! $this->upload->do_upload('file')) {
                        $this->setError($this->upload->display_errors());
                    }else{
                        $file = $this->upload->data();

                        $data['file'] = $uploadPath . $file['file_name'];
                        chmod($data['file'], FILE_READ_MODE);
                    }
                }
                $this->youdid_model->insert($data, true);
            }
        } else {
            $this->setError('Ocorreu um erro ao processar o formulario, tente novamente mais tarde');
        }
        redirect(base_url('sucesso'));
    }

    private function generateMeta($id)
    {
        $id = (int)$id;
        $this->load->helper('html');
        //$this->data['header'] .= meta('fb:app_id', '370117756342202');
        $this->data['header'] .= meta('og:locale', 'pt_BR');
        $this->data['header'] .= meta('og:type', 'article');
        $this->data['header'] .= meta('og:title', $this->data['item'][$id]->title);
        $this->data['header'] .= meta('og:site_name', $this->data['item'][$id]->title);
        $this->data['header'] .= meta('og:description', $this->data['item'][$id]->description);
        $this->data['header'] .= meta('og:image', base_url(str_replace('./assets', 'assets', $this->data['item'][$id]->img)));
    }

    private function generateItem()
    {
        $item = array();

        $item[] = (object)array(
            'title' => 'Crianças',
            'description' => 'Organize visitas a hospitais ou creches próximas, leve livros, jogos ou brincadeiras. As crianças vão adorar.',
            'img' => './assets/img/act/inspiration/botoes_crianca.jpg'
        );
        $item[] = (object)array(
            'title' => 'Animais',
            'description' => 'Promova encontros para recolher gatos ou cachorros abandonados em parques e encaminhar para associações responsáveis. Organize feiras de doação ou divulgue o seu trabalho pelos bichinhos.',
            'img' => './assets/img/act/inspiration/botoes_animais.jpg'
        );
        $item[] = (object)array(
            'title' => 'Idosos',
            'description' => 'Há muitos asilos precisando de voluntários. Organize visitas e leve alegria para os velhinhos que muitas vezes estão lá esquecidos.',
            'img' => './assets/img/act/inspiration/botoes_idosos.jpg'
        );
        $item[] = (object)array(
            'title' => 'Comunidade',
            'description' => 'Organize grupos de voluntários para discutir os problemas do seu bairro. E com a ajuda de todos participe ativamente nas transformações necessárias.',
            'img' => './assets/img/act/inspiration/botoes_comunidade.jpg'
        );
        $item[] = (object)array(
            'title' => 'Praças e parques',
            'description' => 'Convide amigos e voluntários para renovar as praças e parques da sua comunidade. Elas pertencem a todos e cuidar delas é uma responsabilidade coletiva.',
            'img' => './assets/img/act/inspiration/botoes_pracas.jpg'
        );
        $item[] = (object)array(
            'title' => 'Caminhadas e bicicletadas',
            'description' => 'Em busca de pedidos ou anseios locais, organize caminhadas ou bicicletadas para expor suas opiniões e os interesses da sua comunidade.',
            'img' => './assets/img/act/inspiration/botoes_caminhadas.jpg'
        );
        $item[] = (object)array(
            'title' => 'Livros',
            'description' => 'Promova arrecadação de livros para bibliotecas ou crie um espaço para leitura em sua comunidade. Nada é mais importante que o conhecimento.',
            'img' => './assets/img/act/inspiration/botoes_livros.jpg'
        );
        $item[] = (object)array(
            'title' => 'Doação de sangue',
            'description' => 'Reúna seus amigos e conhecidos para doar sangue nos bancos próximos à sua comunidade. Vocês vão salvar vidas e fazer a diferença para quem está em um momento difícil.',
            'img' => './assets/img/act/inspiration/botoes_doacaosangue.jpg'
        );
        $item[] = (object)array(
            'title' => 'O que mais?',
            'description' => 'Todas as ideias são bem-vindas. Então monte o seu projeto e espalhe para os amigos. Contamos com você :)',
            'img' => './assets/img/act/inspiration/botoes_mais.jpg'
        );

        foreach ($item as $key => $row) {
            $row->sanitize = url_title(convert_accented_characters($row->title), '-', true);
            $item[$key] = $row;
        }

        return $item;
    }
}

/* End of file act.php */
/* Location: ./application/controllers/act.php */