<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class news
 */
class News extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('news_model');
    }

    public function index()
    {
        $this->setPageTitle('Notícias');

        $this->data['css'] .= loadCss('news_list');

        $this->data['news'] = $this->news_model->get()->result();

        foreach($this->data['news'] as $key => $row) {
            $row->sanitize = url_title(convert_accented_characters($row->title), '-', true);
            $row->link = './' . $this->uri->segment(1) . '/' . $row->id . '/' . $row->sanitize;
            $this->data['news'][$key] = $row;
        }

        parent::renderer();
    }
    public function detail($id)
    {
        $this->load->model('newsimage_model');
        $this->load->model('newsyoutube_model');
        $this->load->library('youtube');

        $this->data['header'] .= loadFancybox();
        $this->data['news'] = $this->news_model->get(array('id' => $id))->result();
        $this->data['news'] = current($this->data['news']);
        $this->data['news']->date = convertDate($this->data['news']->date);
        $this->data['news']->img = $this->newsimage_model->get(array('news_id' => $id))->result();

        $youtube = $this->newsyoutube_model->get(array('news_id' => $id))->result();

        $this->data['news']->youtube = array();

        foreach($youtube as $row) {
            $this->data['news']->youtube[] = array(
                'url' => $row->url,
                'img' => $this->youtube->getImages($row->url)
            );
        }

        $this->content = $this->router->class . '_' . $this->router->method;
        $this->data['css'] .= loadCss($this->content);

        $this->setPageTitle($this->data['news']->title);


        $this->load->helper('html');
        //$this->data['header'] .= meta('fb:app_id', '370117756342202');
        $this->data['header'] .= meta('og:locale', 'pt_BR');
        $this->data['header'] .= meta('og:type', 'article');
        $this->data['header'] .= meta('og:title', $this->data['news']->title);
        $this->data['header'] .= meta('og:site_name', $this->data['news']->title);
        $this->data['header'] .= meta('og:description', strip_tags($this->data['news']->text));
        $this->data['header'] .= meta('og:image', base_url(str_replace('./assets', 'assets', $this->data['news']->thumb)));

        parent::renderer();
    }
}

/* End of file news.php */
/* Location: ./application/controllers/news.php */