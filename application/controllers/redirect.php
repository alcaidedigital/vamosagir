<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class redirect
 */
class Redirect extends My_Controller
{
    public function page($page)
    {
        switch ($page) {
            case 'inspiracao':
                $page = 'agir/inspiracao';
                break;

            case 'fazendo':
                $page = 'agir/o-que-estamos-fazendo';
                break;

            case 'conte':
                $page = 'agir/conte-o-que-voce-fez';
                break;

            case 'compartilhar':
                $page = 'compartilhe';
                break;

            case 'todos-os-estados-se-reuniram-para-discutir-o-voluntariado':
                $page = 'noticias/1/todos-os-estados-se-reuniram-para-discutir-o-voluntariado';
                break;

            case 'todos-os-estados-se-reuniram-para-discutir-o-voluntariado':
                $page = 'noticias/1/todos-os-estados-se-reuniram-para-discutir-o-voluntariado';
                break;

            case '10-princpios-para-mudar-o-brasil':
                $page = 'noticias/2/10-princpios-para-mudar-o-brasil';
                break;

            case 'fazendo1':
                $page = 'noticias/1/limpeza-e-adesivaco-no-largo-do-machado';
                break;

            case 'agindo':
                $page = 'compartilhe/brasil-agindo';
                break;

            case '7-fatores-para-o-exito-em-acoes-voluntarias':
                $page = 'documentos/1/7-fatores-para-o-exito-em-acoes-voluntarias';
                break;

            case 'o-que-motiva-os-voluntarios':
                $page = 'documentos/2/o-que-motiva-os-voluntarios';
                break;

            case 'caracteristicas-de-um-lider-de-voluntarios-eficiente':
                $page = 'documentos/3/caracteristicas-de-um-lider-de-voluntarios-eficiente';
                break;

            case 'princpios-bsicos-do-voluntariado':
                $page = 'documentos/4/princpios-bsicos-do-voluntariado';
                break;

            case 'material':
                $page = 'material-de-campanha';
                break;

        }
        redirect($page, 'location', 301);
    }
}

/* End of file redirect.php */
/* Location: ./application/controllers/redirect.php */