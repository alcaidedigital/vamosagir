<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class document
 */
class Document extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('document_model');
    }

    public function index()
    {
        $this->setPageTitle('Documentos');

        $this->data['css'] .= loadCss('document_list');

        $this->data['document'] = $this->document_model->get()->result();

        foreach($this->data['document'] as $key => $row) {
            $row->sanitize = url_title(convert_accented_characters($row->title), '-', true);
            $row->link = './' . $this->uri->segment(1) . '/' . $row->id . '/' . $row->sanitize;
            $this->data['document'][$key] = $row;
        }

        parent::renderer();
    }
    public function detail($id)
    {


        $this->data['document'] = $this->document_model->get(array('id' => $id))->result();
        $this->data['document'] = current($this->data['document']);
        $this->content = $this->router->class . '_' . $this->router->method;
        $this->data['css'] .= loadCss($this->content);

        $this->setPageTitle($this->data['document']->title);

        $this->data['document']->date = convertDate($this->data['document']->date);

        $this->load->helper('html');
        //$this->data['header'] .= meta('fb:app_id', '370117756342202');
        $this->data['header'] .= meta('og:locale', 'pt_BR');
        $this->data['header'] .= meta('og:type', 'article');
        $this->data['header'] .= meta('og:title', $this->data['document']->title);
        $this->data['header'] .= meta('og:site_name', $this->data['document']->title);
        $this->data['header'] .= meta('og:description', strip_tags($this->data['document']->text));
        $this->data['header'] .= meta('og:image', base_url(str_replace('./assets', 'assets', $this->data['document']->thumb)));

        parent::renderer();
    }
}

/* End of file document.php */
/* Location: ./application/controllers/document.php */