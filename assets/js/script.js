$(document).ready(function(){
    $('.mob-menu').on('click touchstart', function(e){
        $('#header > .menu').toggleClass('active');
        e.preventDefault();
    });
    //Required attribute fallback
    $('form').submit(function() {
        if (!attributeSupported("required") || ($.browser.safari)) {
            //If required attribute is not supported or browser is Safari (Safari thinks that it has this attribute, but it does not work), then check all fields that has required attribute
            $("form [required]").each(function(index) {
                if (!$(this).val()) {
                    //If at least one required value is empty, then ask to fill all required fields.
                    alert("Please fill all required fields.");
                    return false;
                }
            });
        }
        return false; //This is a test form and I'm not going to submit it
    });
});
//This checks if a specific attribute is supported
function attributeSupported(attribute) {
    return (attribute in document.createElement("input"));
}