
var landing_page = 'vamosagir.com.br';

// start of google analytics tracking 
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-52816184-3', 'auto');
ga('send', 'pageview');
// end of analytics tracking

//<!-- Start of Woopra Code -->
(function(){
    var t,i,e,n=window,o=document,a=arguments,s="script",r=["config","track","identify","visit","push","call"],c=function(){var t,i=this;for(i._e=[],t=0;r.length>t;t++)(function(t){i[t]=function(){return i._e.push([t].concat(Array.prototype.slice.call(arguments,0))),i}})(r[t])};for(n._w=n._w||{},t=0;a.length>t;t++)n._w[a[t]]=n[a[t]]=n[a[t]]||new c;i=o.createElement(s),i.async=1,i.src="//static.woopra.com/js/w.js",e=o.getElementsByTagName(s)[0],e.parentNode.insertBefore(i,e)
})("woopra");

woopra.config({
    domain: 'analytics.brasil.com.vc',
    idle_timeout: 1800000
});

woopra.track('pv', {
    url: location.pathname,
    title: document.title,
    host: location.hostname
});

var form = $('form');
$(form).on('submit', function() {


    var email = $('form input[aria-label*="Email"]').val();
    if (email == '')
        return false;

    woopra.identify({
        name: email,
        email: email,
        source: landing_page
    });

    woopra.track('optin_landing_page');

    return false;

});
//<!-- End of Woopra Code -->