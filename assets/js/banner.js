var images = new Array()
var idSetInterval = null;
var interval = 7000;
var images_background = new Array();

function setBackground(banner){

    images_background = banner;

    changeBackground(-1);

    for (var i = 0; i < images_background.length; i++) {

        images[i] = new Image()
        images[i].src = images_background[i].img;

        var conteudo = "<li style='opacity: 0.3;'><a class='item" + i + "'>" + i + "</a></li>";

        jQuery("#banner>.pagination").append(conteudo);

        jQuery("#banner>.pagination .item" + i).data('index', i);

        jQuery("#banner>.pagination .item" + i).click(function () {
            var index = jQuery(this).data('index') - 1;

            changeBackground(index);

            clearInterval(idSetInterval);

            idSetInterval = setInterval(slideBackground, interval);
        });
    }

    jQuery(jQuery("#banner>.pagination>.item").parent()).css('opacity', 0.8);

    var slideBackground =
        function () {

            var indexImg = jQuery("#banner").data('indexImg');

            changeBackground(indexImg);
        };

    idSetInterval = setInterval(slideBackground, interval);


    function changeBackground(indexImg) {
        jQuery(".pagination li").css('opacity', 0.3);

        ++indexImg;

        var length = images_background.length;

        if (indexImg == length) {
            indexImg = 0;
        }

        jQuery("#banner").css({
            '-webkit-transition': 'background 1s ease-in',
            '-moz-transition': 'background 1s ease-in',
            '-ms-transition': 'background 1s ease-in',
            '-o-transition': 'background 1s ease-in',
            'transition': 'background 1s ease-in'
        });

        jQuery("#banner>.pagination .item" + indexImg).parent().css('opacity', 0.8);

        jQuery("#banner").css('background-image', 'url(' + images_background[indexImg].img + ')');

        if (images_background[indexImg].link) {
            jQuery("#banner>a").prop('href', images_background[indexImg].link)
            jQuery("#banner>a").css('display', '');
        } else {
            jQuery("#banner>a").css('display', 'none');
        }

        jQuery("#banner").data('indexImg', indexImg);
    }
}