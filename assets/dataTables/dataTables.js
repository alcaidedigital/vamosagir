$(function() {
	
	$('.dataTable').dataTable( {
	   'sPaginationType': 'full_numbers',
        'iDisplayLength': 200000000000000,
	   'oLanguage': {
			'sZeroRecords': 'Nada encontrado - desculpe',
			'sInfo': 'Mostrando _START_ até _END_ de _TOTAL_ registros',
			'sSearch': 'Procurar:',
			'sInfoEmpty': 'Mostrando 0 até 0 de 0 registros',
			'sInfoFiltered': '(Filtrado de _MAX_ registros)',
			'oPaginate': {
				'sFirst': 'Primeiro',
				'sPrevious': 'Anterior',
				'sNext': 'Próximo',
				'sLast': 'Último'
			}
		},
		'bRetrieve' : true
		}).find('td').each(function(){
			if($(this).prop('onclick')){
				$(this).css('cursor','pointer');
			}
		});
	$('.dataTables_length').css('display','none');	
});